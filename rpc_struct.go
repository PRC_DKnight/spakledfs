package SparkleDFS

import (
	"time"
)

type ChunkIndex int

//在chunkServer执行的CreateChunk
type CreateChunkArg struct {
	Handle ChunkHandle
}
type CreateChunkReply struct {
	ErrorCode ErrorCode
}

// handshake
type CheckVersionArg struct {
	Handle  ChunkHandle
	Version ChunkVersion
}

//回复我是否没跟上大家的步伐，落后两个版本及以上
type CheckVersionReply struct {
	Overdue bool
}

//在Master执行的
type GetPrimaryAndSecondariesArg struct {
	Handle ChunkHandle
}
type GetPrimaryAndSecondariesReply struct {
	Primary     ServerAddress
	Expire      time.Time
	Secondaries []ServerAddress
}
type MkdirArg struct {
	Path Path
}
type MkdirReply struct {
}
type ReportSelfArg struct {
}
type ReportSelfReply struct {
	Chunks []RPCChunkInfo
}
type RPCChunkInfo struct {
	Handle   ChunkHandle
	Length   Offset // lease Expire time
	Version  ChunkVersion
	Checksum Checksum //校验值
}
type HeartbeatArg struct {
	Address         ServerAddress // chunkserver address
	LeaseExtensions []ChunkHandle // leases to be extended
	AbandonedChunks []ChunkHandle // unrecoverable chunks
}
type HeartbeatReply struct {
	Garbage []ChunkHandle
}

type SendCopyArg struct {
	Handle  ChunkHandle
	Address ServerAddress
}
type SendCopyReply struct {
	ErrorCode ErrorCode
}
type ApplyCopyArg struct {
	Handle  ChunkHandle
	Data    []byte
	Version ChunkVersion
}
type ApplyCopyReply struct {
	ErrorCode ErrorCode
}
type ListArg struct {
	Path Path
}
type ListReply struct {
	DirInfos []PathInfo
}
type CreateFileArg struct {
	Path Path
}
type CreateFileReply struct {
}
type DeleteFileArg struct {
	Path Path
}
type DeleteFileReply struct {
}
type RenameFileArg struct {
	Origin Path
	Target Path
}
type RenameFileReply struct {
}
type GetFileInfoArg struct {
	Path Path
}
type GetFileInfoReply struct {
	IsDir bool
	// if it is a file
	Length int64
	Chunks int64
}

type GetChunkHandleArg struct {
	Path  Path
	Index ChunkIndex
}
type GetChunkHandleReply struct {
	Handle ChunkHandle
}
type ErrorCode int
type GetReplicasArg struct {
	Handle ChunkHandle
}
type GetReplicasReply struct {
	Locations []ServerAddress
}
type ExtendLeaseArg struct {
}
type ExtendLeaseReply struct {
}

type ReadChunkArg struct {
	Handle ChunkHandle
	Offset Offset
	Length int
}
type ReadChunkReply struct {
	Data      []byte
	Length    int
	ErrorCode ErrorCode
}
type WriteChunkArg struct {
	DataID      DataBufferID
	Offset      Offset
	Secondaries []ServerAddress
}
type WriteChunkReply struct {
	ErrorCode ErrorCode
}
type AppendChunkArg struct {
	DataID      DataBufferID
	Secondaries []ServerAddress
}
type AppendChunkReply struct {
	Offset    Offset
	ErrorCode ErrorCode
}
type ApplyMutationArg struct {
	Mtype  MutationType
	DataID DataBufferID
	Offset Offset
}
type ApplyMutationReply struct {
	ErrorCode ErrorCode
}
type ForwardDataArg struct {
	DataID     DataBufferID
	Data       []byte
	ChainOrder []ServerAddress
}
type ForwardDataReply struct {
	ErrorCode ErrorCode
}

const (
	ErrorCode_NameSpace_PathNotExists  = ErrorCode(103)
	ErrorCode_Mkdir_DirAlreadyExists   = ErrorCode(102)
	ErrorCode_Mkdir_FatherRootNotExist = ErrorCode(101)
	ErrorCode_NoneType                 = ErrorCode(100)
	ErrorCode_AppendExceedChunkSize    = ErrorCode(202)

	RPC_ChunkServer_CreateChunk   = "ChunkServer.RPCCreateChunk"
	RPC_ChunkServer_ReadChunk     = "ChunkServer.RPCReadChunk"
	RPC_ChunkServer_WriteChunk    = "ChunkServer.RPCWriteChunk"
	RPC_ChunkServer_SendCopy      = "ChunkServer.RPCSendCopy"
	RPC_ChunkServer_AppendChunk   = "ChunkServer.RPCAppendChunk"
	RPC_ChunkServer_ApplyMutation = "ChunkServer.RPCApplyMutation"
	RPC_ChunkServer_ReportSelf    = "ChunkServer.RPCReportSelf" //master要求chunkserver报告自己拥有的chunk
	RPC_ChunkServer_CheckVersion  = "ChunkServer.RPCCheckVersion"
	RPC_ChunkServer_ApplyCopy     = "ChunkServer.RPCApplyCopy"
	RPC_ChunkServer_ForwardData   = "ChunkServer.RPCForwardData"

	RPC_Master_GetPrimaryAndSecondaries = "Master.RPCGetPrimaryAndSecondaries"
	RPC_Master_Heartbeat                = "Master.RPCHeartbeat"
	RPC_Master_CreateFile               = "Master.RPCCreateFile"
	RPC_Master_DeleteFile               = "Master.RPCDeleteFile"
	RPC_Master_RenameFile               = "Master.RPCRenameFile"
	RPC_Master_Mkdir                    = "Master.RPCMkdir"
	RPC_Master_List                     = "Master.RPCList"
	RPC_Master_GetFileInfo              = "Master.RPCGetFileInfo"
	RPC_Master_GetChunkHandle           = "Master.RPCGetChunkHandle"
	RPC_Master_GetReplicas              = "Master.RPCGetReplicas"
)

// extended error type with error code
type Error struct {
	Code ErrorCode
	Err  string
}

func (e *Error) Error() string {
	return e.Err
}
