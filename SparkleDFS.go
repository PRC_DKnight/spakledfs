package SparkleDFS

//Client 将文件内 offset 翻译为 chunk id + chunk 内 offset。
//Client 与 Master 交互，通过 filename + chunk id + chunk inner offset 获取该 chunk 所有 replica 的位置。Client 会缓存此信息到 filename+chunk id -> replica locations 的字典中。
//Client 于是就近选一个（当然失败了会尝试下一个）replica， 给其发送请求，带上 chunk handle + byte range，读取数据。

//文件系统应该支持文件操作
type ServerAddress string
type ChunkHandle int64
type Path string
type Offset int64
type ChunkVersion int64
type Checksum int64
type ISparkleDFS interface{}

//在GFS下，每一个文件都拆成固定大小的chunk(块)。
//每一个块都由master根据块创建的时间产生一个全局唯一的以后不会改变的64位的chunk handle标志。
//chunkservers在本地磁盘上用Linux文件系统保存这些块，
//并且根据chunk handle和字节区间，通过LInux文件系统读写这些块的数据。
//出于可靠性的考虑，每一个块都会在不同的chunkserver上保存备份。
//缺省情况下，我们保存3个备份，不过用户对于不同的文件namespace区域，指定不同的复制级别。

var ChunkSize = 64 << 20 //64MB
type Chunk struct {
	ChunkID uint64
}

//master节点保存这样三个主要类型的数据：文件和chunk的namespace，文件到chunks的映射关系，每一个chunk的副本的位置。
