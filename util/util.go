package util

import (
	"SparkleDFS"
	"fmt"
	log "github.com/sirupsen/logrus"
	"math/rand"
	"net/rpc"
	"sync"
)

//包装rpc的call方法，可复用rpc连接
func RPCCall(addr SparkleDFS.ServerAddress, methodName string, args, reply interface{}) error {

	var rpcClient *rpc.Client
	//if v, ok := connPool[addr]; ok {
	//	rpcClient = v
	//} else {
	rpcClient, err := rpc.Dial("tcp4", string(addr))
	if err != nil {
		return err
	}
	//connPool[addr] = rpcClient //可能中途断掉了，可以封装一个结构体 包含ShutDown bool值 todo

	//}
	if methodName != SparkleDFS.RPC_Master_Heartbeat {
		log.Infof("RPCCall to %v call method{%v}", addr, methodName)
	}
	err = rpcClient.Call(methodName, args, reply)
	if methodName != SparkleDFS.RPC_Master_Heartbeat {
		log.Infof("RPCCall to %v call method{%v} reply:", addr, methodName, reply)
	}

	return err
}
func RandKinN(k, n int) ([]int, error) {
	if n < k {
		return nil, fmt.Errorf("population is not enough for sampling (n = %d, k = %d)", n, k)
	} //Perm以n个整型数的切片形式返回默认Source的半开区间[0,n)内整数的伪随机排列。
	return rand.Perm(n)[:k], nil
}

func RPCCallAll(addrs []SparkleDFS.ServerAddress, methodName string, args, reply interface{}) error {
	errList := ""
	var wg sync.WaitGroup
	for _, addr := range addrs {
		wg.Add(1)
		go func(addr SparkleDFS.ServerAddress) {
			err := RPCCall(addr, methodName, args, reply)
			wg.Done()
			if err != nil {
				errList += err.Error() + ";"
			}
		}(addr)
	}
	wg.Wait()
	if errList == "" {
		return nil
	} else {
		return fmt.Errorf(errList)
	}

}
