# Google File System的简单实现
目的：学习分布式系统  
参考：
https://static.googleusercontent.com/media/research.google.com/zh-CN//archive/gfs-sosp2003.pdf

https://github.com/merrymercy/goGFS
## 可以改进的方向
## 1. 高扩展性：将模块拆分为独立的分布式系统
### 1.1 拆分目录树、元数据为单独的分布式存储系统
以GFS为例，可以将目录树结构设计为单独的元数据存储分布式系统，可以避免单Master节点的内存瓶颈，支持无限扩展。这样做的缺点是增加了网络开销，可以通过在SDK端做各种缓存来缓解。（拆分元数据存储的例子：  
1. JuiceFS https://github.com/juicedata/juicefs  
2. 字节跳动DanceNN https://mp.weixin.qq.com/s/uJb6iplETFEaO2drL3YF_g）
### 1.2 拆分单机存储系统为单独的分布式存储系统
拆分单机存储系统为单独的分布式存储底座，这个底座可以是文件存储、对象存储、blob存储，而其他直接对接用户的存储系统则在这个底座的基础上进行开发（当作一个库文件使用）。这样做的好处的扩展性好，但会带来设计上的负担，运维上的负担，以及额外的网络开销，这些系统一般部署在一个机房以减轻网络开销的负担，且同一台物理机上可以同时存在ChunkServer和存储底座的实例，但缺点是容灾性差。（分布式存储底座的例子：字节跳动ByteStore）
## 2. 高可用性：
1. 在GFS论文中，Master通过作主从同步来解决单点故障，现代分布式系统一般用如Raft、Paxos等共识算法来实现一组高可用的元数据节点。  
2. 对于ChunkServer而言，可以通过作主备的方式来减少宕机时产生数据迁移从而影响前台IO。
## 3. 数据高可靠：
CRC、EnsureCode
## 4. 性能相关：
1. 将ChunkServer抽象为LSM-Tree实例，一块写入数据的inode、offset、length理解为一个key，数据理解为value，顺理成章地将LSM-Tree的各种优化取舍应用进来，在工程实践中应对多写场景比较常见。  
2. 在SDK端作负载均衡，通过inode+offset来做哈希。  
3. 很多需要交互的元数据可以直接在SDK端作缓存，过期再去询问Master。  
4. 多介质优化，用SSD作上层，HDD作下层。  
5. 预读优化。
