# Simple implementation of Google File System
Goal: To learn about the distributed storage systems.
## Main Features
* +The same structure as the paper
* +Simple coding style

* -Master single point failure
* -Incomplete tests

## Reference:  
https://static.googleusercontent.com/media/research.google.com/zh-CN//archive/gfs-sosp2003.pdf

https://github.com/merrymercy/goGFS

## Modern file systems
## 1. High scalability: Split modules into independent distributed systems
### 1.1 Split the directory tree and metadata into separate distributed storage systems
Taking GFS as an example, the directory tree structure can be designed as a distributed metadata storage system to avoid the memory bottleneck of a single Master node and support unlimited expansion.  The downside of this is the increased network overhead, which can be mitigated by doing various caches on the SDK side.  Example of splitting metadata stores:
1. JuiceFS https://github.com/juicedata/juicefs  
2. ByteDance DanceNN https://mp.weixin.qq.com/s/uJb6iplETFEaO2drL3YF_g).
### 1.2 Split standalone storage systems into separate distributed storage systems
The standalone storage system is split into a single distributed storage base, which can be file storage, object storage, or BLOB storage. Other directly connected storage systems are developed on this base (used as a library file).  These systems are generally deployed in a machine room to reduce the burden of network overhead. In addition, instances of ChunkServer and storage base can exist on the same physical machine, but the disadvantage is poor disaster recovery.  (Example of distributed storage base: ByteStore)
## 2. High availability:
1. In the GFS paper, Master solves single point of failure by Master/slave synchronization. Modern distributed systems generally use consensus algorithms such as Raft and Paxos to implement a set of highly available metadata nodes.
2. As for ChunkServer, it can reduce the data migration caused by downtime and affect the foreground IO by working in active/standby mode.
## 3. High reliability:
CRC, EnsureCode
## 4. Performance:
1. The ChunkServer is abstracted as an LSM-tree instance, and the inode, offset, and length of a block of written data is understood as a key, and the data is understood as a value. Various optimization trade-offs of LSM-Tree are applied logically.
2. Perform load balancing on the SDK side and hash through inode+offset.
3. A lot of metadata that need to be exchanged can be directly cached in the SDK side. After expiration, we will ask the Master.
4. Multi-media optimization, using SSD as the upper layer, HDD as the lower layer.
5. Prefetch optimization.  
