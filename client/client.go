package client

import (
	"SparkleDFS"
	"SparkleDFS/ChunkServer"
	"SparkleDFS/util"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"math/rand"
	"sync"
	"time"
)

type Client struct {
	master SparkleDFS.ServerAddress
	sync.RWMutex
	buffer map[SparkleDFS.ChunkHandle]*SparkleDFS.Lease
	tick   time.Duration
}
type IClient interface {
	//命名空间:
	Create(path SparkleDFS.Path) error
	Delete(path SparkleDFS.Path) error
	Rename(origin SparkleDFS.Path, target SparkleDFS.Path) error
	Mkdir(path SparkleDFS.Path) error
	List(path SparkleDFS.Path) ([]SparkleDFS.PathInfo, error)
	//文件:
	Read(path SparkleDFS.Path, offset SparkleDFS.Offset, data []byte) (n int, err error)
	Write(path SparkleDFS.Path, offset SparkleDFS.Offset, data []byte) error
	Append(path SparkleDFS.Path, data []byte) (offset SparkleDFS.Offset, err error)
	//chunk:
	getChunkHandle(path SparkleDFS.Path, index SparkleDFS.ChunkIndex) (SparkleDFS.ChunkHandle, error)
	readChunk(handle SparkleDFS.ChunkHandle, offset SparkleDFS.Offset, data []byte) (int, error)
	writeChunk(handle SparkleDFS.ChunkHandle, offset SparkleDFS.Offset, data []byte) error
	appendChunk(handle SparkleDFS.ChunkHandle, data []byte) (offset SparkleDFS.Offset, err error)
}

func NewClient(masterAddr SparkleDFS.ServerAddress, leaseBufferTick time.Duration) *Client {
	c := &Client{
		master: masterAddr,
		buffer: make(map[SparkleDFS.ChunkHandle]*SparkleDFS.Lease),
		tick:   leaseBufferTick,
	}
	go func() {
		ticker := time.Tick(c.tick)
		for {
			select {
			case <-ticker:
				now := time.Now()
				c.Lock()
				for id, lease := range c.buffer {
					if lease.Expire.Before(now) {
						delete(c.buffer, id)
					}
				}
				c.Unlock()
			}
		}
	}()
	return c
}
func (c *Client) GetLease(handle SparkleDFS.ChunkHandle) (*SparkleDFS.Lease, error) {
	lease, ok := c.buffer[handle]
	if !ok || lease.Expire.Before(time.Now()) {
		log.Infof("No Cache For Handle:%v,Asking Master", handle)
		var reply SparkleDFS.GetPrimaryAndSecondariesReply
		log.Debug("Client RPC_Master_GetPrimaryAndSecondaries")
		err := util.RPCCall(c.master, SparkleDFS.RPC_Master_GetPrimaryAndSecondaries, SparkleDFS.GetPrimaryAndSecondariesArg{Handle: handle}, &reply)
		log.Debug("Client RPC_Master_GetPrimaryAndSecondaries reply :", reply.Primary, reply.Secondaries, reply.Expire)
		if err != nil {
			return nil, err
		}
		lease = &SparkleDFS.Lease{
			Primary:     reply.Primary,
			Expire:      reply.Expire, //Master已经指定,向Master请求后总是没有过期的
			Secondaries: reply.Secondaries,
		}
		c.buffer[handle] = lease
	}
	return lease, nil
}

func (c *Client) Create(path SparkleDFS.Path) error {
	var reply SparkleDFS.CreateFileReply
	log.Infof("Client RPCCall :{%v},path:{%v}", SparkleDFS.RPC_Master_CreateFile, path)
	err := util.RPCCall(c.master, SparkleDFS.RPC_Master_CreateFile, SparkleDFS.CreateFileArg{Path: path}, &reply)
	log.Infof("Client RPCCall :{%v},path:{%v},err:{%v}", SparkleDFS.RPC_Master_CreateFile, path, err)
	return err
}
func (c *Client) Delete(path SparkleDFS.Path) error {
	var reply SparkleDFS.DeleteFileReply
	log.Infof("Client RPCCall :{%v},path:{%v}", SparkleDFS.RPC_Master_DeleteFile, path)
	err := util.RPCCall(c.master, SparkleDFS.RPC_Master_DeleteFile, SparkleDFS.DeleteFileArg{Path: path}, &reply)
	log.Infof("Client RPCCall :{%v},path:{%v},err:{%v}", SparkleDFS.RPC_Master_DeleteFile, path, err)
	return err
}
func (c *Client) Rename(origin SparkleDFS.Path, target SparkleDFS.Path) error {
	var reply SparkleDFS.RenameFileReply
	log.Infof("Client RPCCall :{%v},origin:{%v},target:{%v},", SparkleDFS.RPC_Master_RenameFile, origin, target)
	err := util.RPCCall(c.master, SparkleDFS.RPC_Master_RenameFile, SparkleDFS.RenameFileArg{Origin: origin, Target: target}, &reply)
	log.Infof("Client RPCCall :{%v},origin:{%v},target:{%v},err:{%v}", SparkleDFS.RPC_Master_DeleteFile, origin, target, err)
	return err
}
func (c *Client) Mkdir(path SparkleDFS.Path) error {
	var reply SparkleDFS.MkdirReply
	log.Infof("Client RPCCall :{%v},path:{%v}", SparkleDFS.RPC_Master_Mkdir, path)
	err := util.RPCCall(c.master, SparkleDFS.RPC_Master_Mkdir, SparkleDFS.MkdirArg{Path: path}, &reply)
	log.Infof("Client RPCCall :{%v},path:{%v},err:{%v}", SparkleDFS.RPC_Master_Mkdir, path, err)
	return err
}
func (c *Client) List(path SparkleDFS.Path) ([]SparkleDFS.PathInfo, error) {
	var reply SparkleDFS.ListReply
	//log.Infof("Client RPCCall :{%v},path:{%v}",SparkleDFS.RPC_Master_List,path)
	err := util.RPCCall(c.master, SparkleDFS.RPC_Master_List, SparkleDFS.MkdirArg{Path: path}, &reply)
	//log.Infof("Client RPCCall :{%v},path:{%v},err:{%v}",SparkleDFS.RPC_Master_List,path,err)
	return reply.DirInfos, err
}

// Read is a client API, read file at specific offset
// it reads up to len(data) bytes form the File. it return the number of bytes and an error.
// the error is set to io.EOF if stream meets the end of file
func (c *Client) Read(path SparkleDFS.Path, offset SparkleDFS.Offset, data []byte) (n int, err error) {
	var reply SparkleDFS.GetFileInfoReply
	err = util.RPCCall(c.master, SparkleDFS.RPC_Master_GetFileInfo, SparkleDFS.GetFileInfoArg{Path: path}, &reply)
	if err != nil {
		return -1, err
	}
	if int64(offset)/reply.Chunks > SparkleDFS.MaxChunkSize {
		return -1, fmt.Errorf("read offset exceeds file size")
	}
	pos := 0
	for pos < len(data) { //直到读满data
		index := offset / SparkleDFS.MaxChunkSize
		if int64(index) >= reply.Chunks {
			err = &SparkleDFS.Error{SparkleDFS.ERRCODE_ReadEOF, "EOF over chunks"}
			break
		}
		chunkOffset := offset % SparkleDFS.MaxChunkSize
		handle, err := c.getChunkHandle(path, SparkleDFS.ChunkIndex(index))
		if err != nil {
			return -1, err
		}

		for {
			n, err = c.readChunk(handle, chunkOffset, data[pos:])
			if err == nil || err.(*SparkleDFS.Error).Code == SparkleDFS.ERRCODE_ReadEOF {
				break
			}
			log.Warning("Read ", handle, " connection error, try again: ", err)
		}

		pos += n
		offset += SparkleDFS.Offset(n)
		if err != nil {
			break
		}
	}
	if err != nil && err.(*SparkleDFS.Error).Code == SparkleDFS.ERRCODE_ReadEOF {
		return pos, io.EOF
	} else {
		return pos, err
	}
}
func (c *Client) Write(path SparkleDFS.Path, offset SparkleDFS.Offset, data []byte) error {
	var reply SparkleDFS.GetFileInfoReply
	err := util.RPCCall(c.master, SparkleDFS.RPC_Master_GetFileInfo, SparkleDFS.GetFileInfoArg{Path: path}, &reply)
	if err != nil {
		return err
	}
	if int64(offset)/reply.Chunks > SparkleDFS.MaxChunkSize {
		return fmt.Errorf("Write offset exceeds file size")
	}

	pos := 0
	for pos < len(data) { //直到写完data
		index := offset / SparkleDFS.MaxChunkSize
		if int64(index) >= reply.Chunks {
			err = &SparkleDFS.Error{SparkleDFS.ERRCODE_ReadEOF, "EOF over chunks"}
			break
		}
		chunkOffset := offset % SparkleDFS.MaxChunkSize
		handle, err := c.getChunkHandle(path, SparkleDFS.ChunkIndex(index))
		if err != nil {
			return err
		}
		DataLenthMax := int(SparkleDFS.MaxChunkSize - chunkOffset)
		var curDataLenth int
		if DataLenthMax+pos > len(data) { //写的数据不超过这个chunk
			curDataLenth = len(data) - pos
		} else {
			curDataLenth = DataLenthMax
		}
		for {
			err := c.writeChunk(handle, chunkOffset, data[pos:pos+curDataLenth])
			if err == nil || err.(*SparkleDFS.Error).Code == SparkleDFS.ERRCODE_ReadEOF {
				break
			}
			log.Warning("Read ", handle, " connection error, try again: ", err)
		}

		pos += curDataLenth
		offset += SparkleDFS.Offset(curDataLenth)
		if pos == len(data) {
			break
		}
	}
	if err != nil && err.(*SparkleDFS.Error).Code == SparkleDFS.ERRCODE_ReadEOF {
		return io.EOF
	} else {
		return err
	}
}
func (c *Client) Append(path SparkleDFS.Path, data []byte) (offset SparkleDFS.Offset, err error) {
	if len(data) > SparkleDFS.MaxAppendSize {
		return 0, fmt.Errorf("len(data) = %v > max append size %v", len(data), SparkleDFS.MaxAppendSize)
	}
	var reply SparkleDFS.GetFileInfoReply
	err = util.RPCCall(c.master, SparkleDFS.RPC_Master_GetFileInfo, SparkleDFS.GetFileInfoArg{Path: path}, &reply)
	if err != nil {
		return -1, err
	}

	start := SparkleDFS.ChunkIndex(reply.Chunks - 1)

	if start < 0 {
		start = 0
	}
	var chunkOffset SparkleDFS.Offset
	for {
		handle, err := c.getChunkHandle(path, start)
		if err != nil {
			return -1, err
		}
		for {
			chunkOffset, err = c.appendChunk(handle, data)
			if err == nil || err.(*SparkleDFS.Error).Code == SparkleDFS.ERRCODE_AppendExceedChunkSize { //跨块了
				break
			}
			log.Warning("Read ", handle, " connection error, try again: ", err)
			time.Sleep(500 * time.Millisecond)
		}
		if err == nil || err.(*SparkleDFS.Error).Code != SparkleDFS.ERRCODE_AppendExceedChunkSize {
			break
		}
		start++
		log.Info("pad this chunk, try on next chunk ", start)
	}
	fileOffset := SparkleDFS.Offset(start)*SparkleDFS.MaxChunkSize + chunkOffset
	return fileOffset, err
}

func (c *Client) getChunkHandle(path SparkleDFS.Path, index SparkleDFS.ChunkIndex) (SparkleDFS.ChunkHandle, error) {
	var reply SparkleDFS.GetChunkHandleReply
	err := util.RPCCall(c.master, SparkleDFS.RPC_Master_GetChunkHandle, SparkleDFS.GetChunkHandleArg{
		Path:  path,
		Index: index,
	}, &reply)
	if err != nil {
		log.Warning("Client getChunkHandle err:", err)
		return -1, err
	}
	return reply.Handle, err
}

//todo 可不可以用buffer
func (c *Client) readChunk(handle SparkleDFS.ChunkHandle, offset SparkleDFS.Offset, data []byte) (int, error) {
	readLen := len(data)
	if SparkleDFS.MaxChunkSize-int(offset) < len(data) {
		readLen = SparkleDFS.MaxChunkSize - int(offset)
	}
	var getReplicaReply SparkleDFS.GetReplicasReply
	err := util.RPCCall(c.master, SparkleDFS.RPC_Master_GetReplicas, SparkleDFS.GetReplicasArg{Handle: handle}, &getReplicaReply)
	if err != nil {
		log.Warning("readChunk RPC_Master_GetReplicas err:", err)
		return -1, &SparkleDFS.Error{SparkleDFS.ERRCODE_UnknownError, err.Error()}
	}
	if len(getReplicaReply.Locations) <= 0 {
		log.Warning("readChunk RPC_Master_GetReplicas err:No Replica", err)
		return -1, &SparkleDFS.Error{SparkleDFS.ERRCODE_UnknownError, err.Error()}
	}
	loc := getReplicaReply.Locations[rand.Intn(len(getReplicaReply.Locations))]
	var readChunkReply SparkleDFS.ReadChunkReply
	readChunkReply.Data = data
	err = util.RPCCall(loc, SparkleDFS.RPC_ChunkServer_ReadChunk, SparkleDFS.ReadChunkArg{Handle: handle, Offset: offset, Length: readLen}, &readChunkReply)
	if err != nil {
		log.Warning("readChunk RPC_ChunkServer_ReadChunk err:", err)
		return -1, err
	}
	if readChunkReply.ErrorCode == SparkleDFS.ERRCODE_ReadEOF {
		return readChunkReply.Length, &SparkleDFS.Error{SparkleDFS.ERRCODE_ReadEOF, "read EOF"}
	}
	return readChunkReply.Length, nil
}

func (c *Client) writeChunk(handle SparkleDFS.ChunkHandle, offset SparkleDFS.Offset, data []byte) error {
	if len(data)+int(offset) > SparkleDFS.MaxChunkSize {
		return fmt.Errorf("len(data)+offset = %v > max chunk size %v", len(data)+int(offset), SparkleDFS.MaxChunkSize)
	}
	lease, err := c.GetLease(handle)
	if err != nil {
		return &SparkleDFS.Error{SparkleDFS.ERRCODE_UnknownError, err.Error()}
	}
	dataID := ChunkServer.NewDataID(handle)
	var forwardDataReply SparkleDFS.ForwardDataReply
	err = util.RPCCall(lease.Primary, SparkleDFS.RPC_ChunkServer_ForwardData, SparkleDFS.ForwardDataArg{
		DataID:     dataID,
		Data:       data,
		ChainOrder: lease.Secondaries,
	}, &forwardDataReply)
	if err != nil {
		return &SparkleDFS.Error{SparkleDFS.ERRCODE_UnknownError, err.Error()}
	}
	wcargs := SparkleDFS.WriteChunkArg{dataID, offset, lease.Secondaries}
	err = util.RPCCall(lease.Primary, SparkleDFS.RPC_ChunkServer_WriteChunk, wcargs, &SparkleDFS.WriteChunkReply{})
	return err
}

//返回写入的offset
func (c *Client) appendChunk(handle SparkleDFS.ChunkHandle, data []byte) (offset SparkleDFS.Offset, err error) {
	if len(data) > SparkleDFS.MaxAppendSize {
		return 0, &SparkleDFS.Error{SparkleDFS.ERRCODE_UnknownError, fmt.Sprintf("len(data) = %v > max append size %v", len(data), SparkleDFS.MaxAppendSize)}
	}

	//log.Infof("Client : get lease ")

	lease, err := c.GetLease(handle)
	if err != nil {
		return -1, &SparkleDFS.Error{SparkleDFS.ERRCODE_UnknownError, err.Error()}
	}
	dataID := ChunkServer.NewDataID(handle)
	var forwardDataReply SparkleDFS.ForwardDataReply

	err = util.RPCCall(lease.Primary, SparkleDFS.RPC_ChunkServer_ForwardData, SparkleDFS.ForwardDataArg{
		DataID:     dataID,
		Data:       data,
		ChainOrder: lease.Secondaries,
	}, &forwardDataReply)
	if err != nil {
		return -1, err
	}
	var appendReply SparkleDFS.AppendChunkReply
	appendArgs := &SparkleDFS.AppendChunkArg{DataID: dataID, Secondaries: lease.Secondaries}

	err = util.RPCCall(lease.Primary, SparkleDFS.RPC_ChunkServer_AppendChunk, appendArgs, &appendReply)
	if err != nil {
		return -1, &SparkleDFS.Error{SparkleDFS.ERRCODE_UnknownError, err.Error()}
	}
	if appendReply.ErrorCode == SparkleDFS.ErrorCode_AppendExceedChunkSize { //写入跨块了！chunkserve已经填充最后的块，请重试以便在新块写入
		return appendReply.Offset, &SparkleDFS.Error{appendReply.ErrorCode, "append over chunks"}
	}
	return appendReply.Offset, nil
}
