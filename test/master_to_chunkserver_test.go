package master

import (
	"SparkleDFS"
	"SparkleDFS/ChunkServer"
	"SparkleDFS/util"
	"fmt"
	"testing"
	"time"
)

const masterAddr = SparkleDFS.ServerAddress("0.0.0.0:5555")

func Test_Master_RPCMkdir(t *testing.T) {
	var reply SparkleDFS.MkdirReply
	err := util.RPCCall(masterAddr, SparkleDFS.RPC_Master_Mkdir, SparkleDFS.MkdirArg{Path: "/root"}, &reply)
	fmt.Println(err)
	err = util.RPCCall(masterAddr, SparkleDFS.RPC_Master_Mkdir, SparkleDFS.MkdirArg{Path: "/docker"}, &reply)
	fmt.Println(err)
	err = util.RPCCall(masterAddr, SparkleDFS.RPC_Master_Mkdir, SparkleDFS.MkdirArg{Path: "/root/a1"}, &reply)
	fmt.Println(err)
	err = util.RPCCall(masterAddr, SparkleDFS.RPC_Master_Mkdir, SparkleDFS.MkdirArg{Path: "/root/a1"}, &reply)
	fmt.Println(err)
	err = util.RPCCall(masterAddr, SparkleDFS.RPC_Master_Mkdir, SparkleDFS.MkdirArg{Path: "/root/a3"}, &reply)
	fmt.Println(err)
	err = util.RPCCall(masterAddr, SparkleDFS.RPC_Master_Mkdir, SparkleDFS.MkdirArg{Path: "/root/a2"}, &reply)
	fmt.Println(err)
}
func Test_Master_RPCCreatefile(t *testing.T) {
	var reply SparkleDFS.CreateFileReply
	err := util.RPCCall(masterAddr, SparkleDFS.RPC_Master_CreateFile, SparkleDFS.CreateFileArg{Path: "/root/test.db"}, &reply)
	fmt.Println(err)
}
func Test_Master_RPCDeleteFile(t *testing.T) {
	var reply SparkleDFS.DeleteFileReply
	err := util.RPCCall(masterAddr, SparkleDFS.RPC_Master_DeleteFile, SparkleDFS.DeleteFileArg{Path: "/root/test.db"}, &reply)
	fmt.Println(err)
}
func Test_Master_RPCList(t *testing.T) {
	var reply SparkleDFS.ListReply
	err := util.RPCCall(masterAddr, SparkleDFS.RPC_Master_List, SparkleDFS.ListArg{Path: "/root"}, &reply)
	fmt.Println(reply.DirInfos)
	fmt.Println(err)
}
func Test_ss(t *testing.T) {
	cs2 := ChunkServer.NewChunkServer("0.0.0.0:5554", "0.0.0.0:5555", "./cs1")
	ticker := time.Tick(10 * time.Second)
	for {
		select {
		case <-ticker:
			fmt.Println(cs2)
		}
	}
}
