package master

import (
	"SparkleDFS"
	"SparkleDFS/client"
	"fmt"
	"testing"
)

func Test_Mkdir(t *testing.T) {
	c := client.NewClient(masterAddr, SparkleDFS.LeaseBufferTick)
	c.Mkdir("/root")
	c.Create("/root/abc.txt")
	writeData := "阿拉啦啦啦"
	offset, err := c.Append("/root/abc.txt", []byte(writeData))
	data := make([]byte, len(writeData))
	c.Read("/root/abc.txt", offset, data)
	fmt.Println(offset)
	fmt.Println(err)
	fmt.Println("读取已写入的数据结果：", string(data))
}
