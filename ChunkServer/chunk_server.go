package ChunkServer

import (
	"SparkleDFS"
	"SparkleDFS/util"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"net"
	"net/rpc"
	"os"
	"path"
	"sync"
	"time"
)

type ChunkServer struct {
	sync.RWMutex
	address         SparkleDFS.ServerAddress
	master          SparkleDFS.ServerAddress
	storageRootPath string
	l               net.Listener
	shutdown        chan struct{}
	dead            bool

	Chunks                 map[SparkleDFS.ChunkHandle]*chunkInfo `json:"chunks"`
	garbage                []SparkleDFS.ChunkHandle
	dlBuf                  *downloadBuffer // expiring download buffer todo 怎么用，有啥用
	pendingLeaseExtensions *util.ArraySet  // pending lease extension
}

type chunkInfo struct {
	sync.RWMutex
	Length    SparkleDFS.Offset `json:"length"`
	expire    time.Time
	CheckSum  SparkleDFS.Checksum                   `json:"check_sum"`
	Version   SparkleDFS.ChunkVersion               `json:"version"`
	mutations map[SparkleDFS.ChunkVersion]*Mutation // mutation buffer todo 记录版本的改变，相当于log？
	abandoned bool                                  // unrecoverable error
}

type Mutation struct {
	mtype  SparkleDFS.MutationType
	data   []byte
	offset SparkleDFS.Offset
}
type IChunkServer interface {
	heartbeat() error
	garbageCollection() error
	loadMeta() error
	storeMeta() error
	Shutdown()

	writeChunk(handle SparkleDFS.ChunkHandle, data []byte, offset SparkleDFS.Offset) error
	readChunk(handle SparkleDFS.ChunkHandle, offset SparkleDFS.Offset, data []byte) (int, error)
	deleteChunk(handle SparkleDFS.ChunkHandle) error
	doMutation(handle SparkleDFS.ChunkHandle, m *Mutation) error

	RPCReportSelf(args SparkleDFS.ReportSelfArg, reply *SparkleDFS.ReportSelfReply) error
	RPCCreateChunk(args SparkleDFS.CreateChunkArg, reply *SparkleDFS.CreateChunkReply) error
	RPCReadChunk(args SparkleDFS.ReadChunkArg, reply *SparkleDFS.ReadChunkReply) error
	RPCWriteChunk(args SparkleDFS.WriteChunkArg, reply *SparkleDFS.WriteChunkReply) error
	RPCAppendChunk(args SparkleDFS.AppendChunkArg, reply *SparkleDFS.AppendChunkReply) error

	RPCCheckVersion(args SparkleDFS.CheckVersionArg, reply *SparkleDFS.CheckVersionReply) error
	RPCForwardData(args SparkleDFS.ForwardDataArg, reply *SparkleDFS.ForwardDataReply) error
	RPCApplyMutation(args SparkleDFS.ApplyMutationArg, reply *SparkleDFS.ApplyMutationReply) error
	RPCSendCopy(args SparkleDFS.SendCopyArg, reply *SparkleDFS.SendCopyReply) error
	RPCApplyCopy(args SparkleDFS.ApplyCopyArg, reply *SparkleDFS.ApplyCopyReply) error
}

func NewChunkServer(addr, masterAddr SparkleDFS.ServerAddress, rootDir string) *ChunkServer {
	cs := &ChunkServer{
		address:                addr,
		master:                 masterAddr,
		storageRootPath:        rootDir,
		shutdown:               make(chan struct{}),
		dead:                   false,
		Chunks:                 make(map[SparkleDFS.ChunkHandle]*chunkInfo),
		dlBuf:                  newDownloadBuffer(SparkleDFS.DownloadBufferExpire, SparkleDFS.DownloadBufferTick),
		pendingLeaseExtensions: new(util.ArraySet),
	}
	l, err := net.Listen("tcp4", string(addr))
	if err != nil {
		log.Fatal("chunkserver listen error:", err)
	}
	cs.l = l
	rpcs := rpc.NewServer()
	rpcs.Register(cs)
	// Mkdir
	_, err = os.Stat(rootDir) //Stat返回一个描述命名文件的FileInfo。如果出现错误，则其类型为PathError
	if err != nil {           // not exist
		err := os.Mkdir(rootDir, SparkleDFS.FilePerm)
		if err != nil {
			log.Fatal("error in mkdir ", err)
		}
	}
	err = cs.loadMeta()
	if err != nil {
		log.Warning("Error in load metadata: ", err)
	}
	// RPC Handler
	go func() {
		for {
			select {
			case <-cs.shutdown:
				return
			default:
			}
			conn, err := cs.l.Accept()
			if err == nil {
				go func() {
					rpcs.ServeConn(conn)
					conn.Close()
				}()
			} else {
				if !cs.dead {
					log.Fatal("chunkserver accept error: ", err)
				}
			}
		}
	}()
	// Background Activity
	// heartbeat, store persistent meta, garbage collection ...
	go func() {
		heartbeatTicker := time.Tick(SparkleDFS.HeartbeatInterval)
		storeTicker := time.Tick(SparkleDFS.ServerStoreInterval)
		garbageTicker := time.Tick(SparkleDFS.GarbageCollectionInt)
		quickStart := make(chan bool, 1) // send first heartbeat right away..
		quickStart <- true
		for {
			var err error
			var branch string
			select {
			case <-cs.shutdown:
				return
			case <-quickStart:
				branch = "heartbeat"
				err = cs.heartbeat()
			case <-heartbeatTicker:
				branch = "heartbeat"
				err = cs.heartbeat()
			case <-storeTicker:
				branch = "storemeta"
				err = cs.storeMeta()
			case <-garbageTicker:
				branch = "garbagecollecton"
				err = cs.garbageCollection()
			}

			if err != nil {
				log.Errorf("%v background(%v) error %v", cs.address, branch, err)
			}
		}
	}()

	log.Infof("ChunkServer is now running. addr = %v, root path = %v, master addr = %v", addr, rootDir, masterAddr)

	return cs
}

const (
	MetaFileName = "chunk-server.meta"
	FilePerm     = 0755
)

func (cs *ChunkServer) storeMeta() error {
	metaFile, err := os.OpenFile(path.Join(cs.storageRootPath, MetaFileName), os.O_CREATE|os.O_WRONLY, SparkleDFS.FilePerm)
	defer metaFile.Close()
	if err != nil {
		return err
	}
	encoder := json.NewEncoder(metaFile)
	return encoder.Encode(cs)
}
func (cs *ChunkServer) loadMeta() error {
	metaFile, err := os.OpenFile(path.Join(cs.storageRootPath, MetaFileName), os.O_CREATE|os.O_RDONLY, SparkleDFS.FilePerm)
	if err != nil {
		return err
	}
	defer metaFile.Close()
	decoder := json.NewDecoder(metaFile)
	return decoder.Decode(cs)
}
func (cs *ChunkServer) Shutdown() {
	if !cs.dead {
		log.Warning(cs.address, " Shutdown")
		cs.dead = true
		close(cs.shutdown)
		cs.l.Close()
	}
	err := cs.storeMeta()
	if err != nil {
		log.Warning("error in store metadeta: ", err)
	}
}

//heartbeat 发送chunkserver自己的地址以及需要续约的chunkhandle
func (cs *ChunkServer) heartbeat() error {
	ple := cs.pendingLeaseExtensions.GetAllAndClear()
	var list []SparkleDFS.ChunkHandle
	for _, v := range ple {
		list = append(list, v.(SparkleDFS.ChunkHandle))
	}
	heartbeatArg := SparkleDFS.HeartbeatArg{
		Address:         cs.address,
		LeaseExtensions: list,
	}
	var reply SparkleDFS.HeartbeatReply
	err := util.RPCCall(cs.master, SparkleDFS.RPC_Master_Heartbeat, heartbeatArg, &reply) // todo 通信失败应该报告吗
	if err != nil {
		log.Warning("ChunkServer heartbeat RPCCall err:", err)
		return err
	}
	cs.garbage = append(cs.garbage, reply.Garbage...)
	return nil
}

//垃圾收集注意:不需要锁，因为后台活动是单线程的
func (cs *ChunkServer) garbageCollection() error {
	for _, v := range cs.garbage {
		cs.deleteChunk(v)
	}

	cs.garbage = make([]SparkleDFS.ChunkHandle, 0)
	return nil
}

//向master报告自己拥有的chunk的信息，
func (cs *ChunkServer) RPCReportSelf(args SparkleDFS.ReportSelfArg, reply *SparkleDFS.ReportSelfReply) error {
	cs.RLock()
	defer cs.RUnlock()
	log.Debug(cs.address, " report collect start")
	var re []SparkleDFS.RPCChunkInfo
	for k, v := range cs.Chunks {
		re = append(reply.Chunks, SparkleDFS.RPCChunkInfo{
			Handle:   k,
			Length:   v.Length,
			Version:  v.Version,
			Checksum: v.CheckSum,
		})
	}
	reply.Chunks = re
	log.Debug(cs.address, " report collect start")
	return nil
}
func (cs *ChunkServer) writeChunk(handle SparkleDFS.ChunkHandle, data []byte, offset SparkleDFS.Offset) error {
	cs.RLock()
	ck := cs.Chunks[handle]
	cs.RUnlock()
	newLen := offset + SparkleDFS.Offset(len(data))
	if newLen > ck.Length {
		ck.Length = newLen
	}
	if newLen > SparkleDFS.MaxChunkSize {
		log.Fatal("ChunkServer writeChunk newLen is bigger than MaxChunkSize")
	}

	file, err := os.OpenFile(cs.chunkFileName(handle), os.O_WRONLY|os.O_CREATE, FilePerm)
	if err != nil {
		log.Warning("ChunkServer writeChunk OpenFile err:", err)
		return err
	}
	defer file.Close()
	_, err = file.WriteAt(data, int64(offset))
	if err != nil {
		log.Warning("ChunkServer writeChunk WriteAt err:", err)
		return err
	}
	log.Infof("Server %v : write chunk %v at %v len %v", cs.address, handle, offset, len(data))
	return nil
}
func (cs *ChunkServer) chunkFileName(handle SparkleDFS.ChunkHandle) string {
	return path.Join(cs.storageRootPath, fmt.Sprintf("chunk%v.chk", handle))
}

func (cs *ChunkServer) readChunk(handle SparkleDFS.ChunkHandle, offset SparkleDFS.Offset, data []byte) (int, error) {
	f, err := os.Open(cs.chunkFileName(handle))
	if err != nil {
		return -1, err
	}
	defer f.Close()

	log.Infof("Server %v : read chunk %v at %v len %v", cs.address, handle, offset, len(data))
	return f.ReadAt(data, int64(offset))
}
func (cs *ChunkServer) deleteChunk(handle SparkleDFS.ChunkHandle) error {
	cs.Lock()
	delete(cs.Chunks, handle)
	cs.Unlock()
	return os.Remove(cs.chunkFileName(handle))
}

// apply mutations (write, append, pad) in chunk buffer in proper order according to version number
//上层调用提供锁
func (cs *ChunkServer) doMutation(handle SparkleDFS.ChunkHandle, m *Mutation) error {
	var err error
	switch m.mtype {
	case SparkleDFS.MutationAppend: //要用锁，不然无法实现 append at least once
		err = cs.writeChunk(handle, m.data, m.offset)
	case SparkleDFS.MutationWrite:
		err = cs.writeChunk(handle, m.data, m.offset)
	case SparkleDFS.MutationFillChunk:
		data := []byte{0}
		err = cs.writeChunk(handle, data, SparkleDFS.MaxChunkSize-1)
	}
	if err != nil {
		cs.RLock()
		ck := cs.Chunks[handle]
		cs.RUnlock()
		log.Warning("ChunkServer doMutation err:", err)
		ck.abandoned = true
	}
	return err
}
func (cs *ChunkServer) RPCCreateChunk(args SparkleDFS.CreateChunkArg, reply *SparkleDFS.CreateChunkReply) error {
	cs.Lock()
	defer cs.Unlock()
	log.Infof("Server %v : create chunk %v", cs.address, args.Handle)
	if _, ok := cs.Chunks[args.Handle]; ok {
		log.Warning("recreate chunkHandle:", args.Handle)
		return fmt.Errorf("Chunk %v already exists", args.Handle)
	}
	cs.Chunks[args.Handle] = &chunkInfo{
		Length: 0,
	}
	filename := path.Join(cs.storageRootPath, fmt.Sprintf("chunk%v.chk", args.Handle))
	_, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	return nil
}

func (cs *ChunkServer) RPCReadChunk(args SparkleDFS.ReadChunkArg, reply *SparkleDFS.ReadChunkReply) error {
	cs.RLock()
	ck, ok := cs.Chunks[args.Handle]
	if !ok || ck.abandoned {
		log.Warningf("Read a chunk{%v} which is not exists:", args.Handle)
		return fmt.Errorf("Read a chunk{%v} which is not exists:", args.Handle)
	}
	cs.RUnlock()
	var err error
	reply.Data = make([]byte, args.Length)
	log.Infof("Server %v : readChunk %v", cs.address, args.Handle)
	ck.RLock()
	reply.Length, err = cs.readChunk(args.Handle, args.Offset, reply.Data)
	ck.RUnlock()
	if err == io.EOF {
		reply.ErrorCode = SparkleDFS.ERRCODE_ReadEOF
		return nil
	}
	return err

}

// RPCWriteChunk 被客户端调用,从dlbuf取出data，domutation，之后再rpc叫副本服务器domutation
// applies chunk write to itself (primary) and asks secondaries to do the same.
func (cs *ChunkServer) RPCWriteChunk(args SparkleDFS.WriteChunkArg, reply *SparkleDFS.WriteChunkReply) error {
	data, err := cs.dlBuf.Fetch(args.DataID) //取出dlbuf对应的数据
	if err != nil {
		return err
	}
	newLen := args.Offset + SparkleDFS.Offset(len(data))
	if newLen > SparkleDFS.MaxChunkSize {
		return fmt.Errorf("writeChunk new length is too large. Size %v > MaxSize %v", len(data), SparkleDFS.MaxChunkSize)
	}
	cs.RLock()
	ck, ok := cs.Chunks[args.DataID.Handle]
	cs.RUnlock()
	if !ok || ck.abandoned {
		log.Warningf("RPCWriteChunk{%v} which is not exists:", args.DataID.Handle)
		return fmt.Errorf("RPCWriteChunk{%v} which is not exists:", args.DataID.Handle)
	}
	mutation := &Mutation{
		mtype:  SparkleDFS.MutationWrite,
		data:   data,
		offset: args.Offset,
	}
	callArgs := &SparkleDFS.ApplyMutationArg{
		Mtype:  mutation.mtype,
		DataID: args.DataID,
		Offset: args.Offset,
	}
	var re SparkleDFS.ApplyMutationReply
	err = cs.doMutation(args.DataID.Handle, mutation)
	if err != nil {
		return err
	}
	return util.RPCCallAll(args.Secondaries, SparkleDFS.RPC_ChunkServer_ApplyMutation, callArgs, &re)
}

//RPCAppendChunk 客户端调用RPCAppendChunk来应用原子记录追加。
//数据的长度应该在1/4 chunk大小之内。如果附加数据后的块大小将超过限制，填充当前块并请求客户端重试下一个块。
//数据的长度应该在1/4 chunk大小之内，避免开新chunk浪费太多
func (cs *ChunkServer) RPCAppendChunk(args SparkleDFS.AppendChunkArg, reply *SparkleDFS.AppendChunkReply) error {
	data, err := cs.dlBuf.Fetch(args.DataID)
	if err != nil {
		return err
	}

	if len(data) > SparkleDFS.MaxAppendSize {
		return fmt.Errorf("Append data size %v excceeds max append size %v", len(data), SparkleDFS.MaxAppendSize)
	}

	handle := args.DataID.Handle
	cs.RLock()
	ck, ok := cs.Chunks[handle]
	cs.RUnlock()
	if !ok || ck.abandoned {
		return fmt.Errorf("Chunk %v does not exist or is abandoned", handle)
	}
	newLen := ck.Length + SparkleDFS.Offset(len(data))
	offset := ck.Length
	var mtype SparkleDFS.MutationType
	if newLen > SparkleDFS.MaxChunkSize {
		mtype = SparkleDFS.MutationFillChunk
		ck.Length = SparkleDFS.MaxChunkSize
		reply.ErrorCode = SparkleDFS.ErrorCode_AppendExceedChunkSize
	} else {
		mtype = SparkleDFS.MutationAppend
		ck.Length = newLen
	}
	mutation := &Mutation{
		mtype:  mtype,
		data:   data,
		offset: offset,
	}
	callArgs := &SparkleDFS.ApplyMutationArg{
		Mtype:  mutation.mtype,
		DataID: args.DataID,
		Offset: offset,
	}
	var re SparkleDFS.ApplyMutationReply
	err = cs.doMutation(args.DataID.Handle, mutation)
	if err != nil {
		return err
	}
	reply.Offset = offset
	return util.RPCCallAll(args.Secondaries, SparkleDFS.RPC_ChunkServer_ApplyMutation, callArgs, &re)
}

func (cs *ChunkServer) RPCCheckVersion(args SparkleDFS.CheckVersionArg, reply *SparkleDFS.CheckVersionReply) error {
	cs.RLock()
	ck, ok := cs.Chunks[args.Handle]
	cs.RUnlock()
	if !ok || ck.abandoned {
		return fmt.Errorf("cannot find chunk %v", args.Handle)
	}
	if ck.Version+1 == args.Version { //因为租约过期，所以master版本已经+了1，这是正常更新流程
		ck.Version++
		reply.Overdue = false
	} else { //落后版本,直接废弃
		log.Warningf("%v : Overdue chunk %v", cs.address, args.Handle)
		ck.abandoned = true
		reply.Overdue = true
	}
	return nil
}
func (cs *ChunkServer) RPCForwardData(args SparkleDFS.ForwardDataArg, reply *SparkleDFS.ForwardDataReply) error {
	if _, ok := cs.dlBuf.Get(args.DataID); ok {
		log.Warningf("RPCForwardData DataID{%v} is already exists in server{%v}", args.DataID, cs.address)
		return fmt.Errorf("RPCForwardData DataID{%v} is already exists in server{%v}", args.DataID, cs.address)
	}
	cs.dlBuf.Set(args.DataID, args.Data)
	if len(args.ChainOrder) <= 0 {
		return nil
	}
	next := args.ChainOrder[0]
	args.ChainOrder = args.ChainOrder[1:]

	return util.RPCCall(next, SparkleDFS.RPC_ChunkServer_ForwardData, args, reply)

}
func (cs *ChunkServer) RPCApplyMutation(args SparkleDFS.ApplyMutationArg, reply *SparkleDFS.ApplyMutationReply) error {
	data, err := cs.dlBuf.Fetch(args.DataID)
	if err != nil {
		return err
	}
	handle := args.DataID.Handle
	cs.RLock()
	ck, ok := cs.Chunks[handle]
	cs.RUnlock()
	if !ok || ck.abandoned {
		return fmt.Errorf("cannot find chunk %v", handle)
	}
	//log.Infof("Server %v : get mutation to chunk %v version %v", cs.address, handle, args.Version)
	mutation := &Mutation{args.Mtype, data, args.Offset}
	ck.Lock()
	defer ck.Unlock()
	return cs.doMutation(handle, mutation)
}

// rpcsendcopy被master调用，发送整个副本到给定的地址rpcsendcopy被master调用，发送整个副本到给定的地址
func (cs *ChunkServer) RPCSendCopy(args SparkleDFS.SendCopyArg, reply *SparkleDFS.SendCopyReply) error {
	handle := args.Handle
	cs.RLock()
	ck, ok := cs.Chunks[handle]
	cs.RUnlock()
	if !ok || ck.abandoned {
		return fmt.Errorf("Chunk %v does not exist or is abandoned", handle)
	}

	ck.RLock()
	defer ck.RUnlock()

	log.Infof("Server %v : Send copy of %v to %v", cs.address, handle, args.Address)
	data := make([]byte, ck.Length)
	_, err := cs.readChunk(handle, 0, data)
	if err != nil {
		return err
	}

	var r SparkleDFS.ApplyCopyReply
	return util.RPCCall(args.Address, SparkleDFS.RPC_ChunkServer_ApplyCopy, SparkleDFS.ApplyCopyArg{handle, data, ck.Version}, &r)

}

//RPCApplyCopy 由另一个副本调用，将本地版本重写为给定的副本数据,要求被调用服务器上必须本来就存在这个chunkHandle，只是可能不是最新版
func (cs *ChunkServer) RPCApplyCopy(args SparkleDFS.ApplyCopyArg, reply *SparkleDFS.ApplyCopyReply) error {
	handle := args.Handle
	cs.RLock()
	ck, ok := cs.Chunks[handle]
	cs.RUnlock()
	if !ok || ck.abandoned {
		return fmt.Errorf("Chunk %v does not exist or is abandoned", handle)
	}

	ck.Lock()
	defer ck.Unlock()

	log.Infof("Server %v : Apply copy of %v", cs.address, handle)

	ck.Version = args.Version
	err := cs.writeChunk(handle, args.Data, 0)
	if err != nil {
		return err
	}
	log.Infof("Server %v : Apply done", cs.address)
	return nil

}
