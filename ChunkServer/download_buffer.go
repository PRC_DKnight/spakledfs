package ChunkServer

import (
	"SparkleDFS"
	"fmt"
	"sync"
	"time"
)

type downloadItem struct {
	data   []byte
	expire time.Time
}

type downloadBuffer struct {
	sync.RWMutex
	buffer map[SparkleDFS.DataBufferID]downloadItem
	expire time.Duration
	tick   time.Duration
}

//newDownloadBuffer返回一个downloadBuffer。默认过期时间为expire。downloadBuffer会在每一个滴答声中清理过期的项目。
func newDownloadBuffer(expire, tick time.Duration) *downloadBuffer {
	buf := &downloadBuffer{
		buffer: make(map[SparkleDFS.DataBufferID]downloadItem),
		expire: expire,
		tick:   tick,
	}
	// cleanup
	go func() {
		ticker := time.Tick(tick)
		for {
			<-ticker
			now := time.Now()
			buf.Lock()
			for id, item := range buf.buffer {
				if item.expire.Before(now) {
					delete(buf.buffer, id)
				}
			}
			buf.Unlock()
		}
	}()
	return buf
}

// NewDataID allocate a new DataID for given handle
func NewDataID(handle SparkleDFS.ChunkHandle) SparkleDFS.DataBufferID {
	now := time.Now()
	timeStamp := now.Nanosecond() + now.Second()*1000 + now.Minute()*60*1000
	return SparkleDFS.DataBufferID{handle, timeStamp}
}

//Set 设置DataBuffer的数据，以及默认的过期时刻
func (buf *downloadBuffer) Set(id SparkleDFS.DataBufferID, data []byte) {
	buf.Lock()
	defer buf.Unlock()
	buf.buffer[id] = downloadItem{data, time.Now().Add(buf.expire)}
}

//Get 返回该DataBufferID的byte数据
func (buf *downloadBuffer) Get(id SparkleDFS.DataBufferID) ([]byte, bool) {
	buf.Lock()
	defer buf.Unlock()
	item, ok := buf.buffer[id]
	if !ok {
		return nil, ok
	}
	item.expire = time.Now().Add(buf.expire) // touch
	return item.data, ok
}

//Fetch 删除该DataBuffer并返回该DataBuffer的byte数据
func (buf *downloadBuffer) Fetch(id SparkleDFS.DataBufferID) ([]byte, error) {
	buf.Lock()
	defer buf.Unlock()

	item, ok := buf.buffer[id]
	if !ok {
		return nil, fmt.Errorf("DataID %v not found in download buffer.", id)
	}

	delete(buf.buffer, id)
	return item.data, nil
}

func (buf *downloadBuffer) Delete(id SparkleDFS.DataBufferID) {
	buf.Lock()
	defer buf.Unlock()
	delete(buf.buffer, id)
}
