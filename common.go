package SparkleDFS

import "time"

type MutationType int

type DataBufferID struct {
	Handle    ChunkHandle
	TimeStamp int
}

const (
	MutationWrite = iota
	MutationAppend
	MutationFillChunk
	DeletedFilePrefix    = "__del__"
	MaxAppendSize        = MaxChunkSize / 4
	MaxChunkSize         = 32 << 20 // 512KB DEBUG ONLY 64 << 20
	HeartbeatInterval    = 1000 * time.Millisecond
	ServerStoreInterval  = 10 * time.Second // 30 * time.Minute
	GarbageCollectionInt = 30 * time.Hour   // 1 * time.Day
	DownloadBufferExpire = 2 * time.Minute
	DownloadBufferTick   = 30 * time.Second
	LeaseBufferTick      = 500 * time.Millisecond
	// master
	Time_ServerCheckInterval = 400 * time.Millisecond //
	Time_MasterStoreInterval = 10 * time.Second       // 30 * time.Minute 30 * time.Hour
	Num_MinOfReplicas        = 3
	Time_ServerTimeOut       = 1 * time.Second

	Time_LeaseExpire              = 3 * time.Second
	ERRCODE_UnknownError          = 202
	ERRCODE_Timeout               = 203
	ERRCODE_AppendExceedChunkSize = 204
	ERRCODE_WriteExceedChunkSize  = 205
	ERRCODE_ReadEOF               = 206
	ERRCODE_NotAvailableForCopy   = 207
	LeaseExpire                   = 3 * time.Second //1 * time.Minute
	ServerTimeout                 = 1 * time.Second
	ClientTryTimeout              = 2*LeaseExpire + 3*ServerTimeout
)

type PathInfo struct { //是一个代表空间树节点的数据结构，而不是实例
	Name string
	// if it is a directory
	IsDir bool
	// if it is a file
	Length int64
	Chunks int64
}

const FilePerm = 0777

type Lease struct {
	Primary     ServerAddress
	Expire      time.Time
	Secondaries []ServerAddress
}
