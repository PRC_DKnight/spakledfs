package master

import (
	"SparkleDFS"
	"SparkleDFS/util"
	"fmt"
	log "github.com/sirupsen/logrus"
	"sync"
	"time"
)

//todo ??

type ChunkManager struct {
	sync.RWMutex
	Chunks           map[SparkleDFS.ChunkHandle]*chunkInfo
	Files            map[SparkleDFS.Path]*FileInfo
	CurChunkHandle   SparkleDFS.ChunkHandle   //当前分配到的id autoincrement
	replicasNeedList []SparkleDFS.ChunkHandle // list of Handles need a new replicas
	// (happends when some servers are disconneted)
}

type IChunkManager interface {
	CreateChunk(path SparkleDFS.Path, addrs []SparkleDFS.ServerAddress) (SparkleDFS.ChunkHandle, []SparkleDFS.ServerAddress, error)
	GetChunk(path SparkleDFS.Path, index SparkleDFS.ChunkIndex) (SparkleDFS.ChunkHandle, error)
	RemoveChunks(handles []SparkleDFS.ChunkHandle, server SparkleDFS.ServerAddress) error

	RegisterReplica(handle SparkleDFS.ChunkHandle, addr SparkleDFS.ServerAddress) error
	GetReplicas(handle SparkleDFS.ChunkHandle) ([]SparkleDFS.ServerAddress, error)

	GetLeaseHolder(handle SparkleDFS.ChunkHandle) (*SparkleDFS.Lease, []SparkleDFS.ServerAddress, error)
	//ExtendLease(handle ChunkHandle, Primary ServerAddress) error

	//GetReplicasNeedList() []ChunkHandle
}

func newChunkManager() *ChunkManager {
	cm := &ChunkManager{
		Chunks: make(map[SparkleDFS.ChunkHandle]*chunkInfo),
		Files:  make(map[SparkleDFS.Path]*FileInfo),
	}
	log.Info("-----------new chunk manager")
	return cm
}

type FileInfo struct { //文件信息 被拆成了那些chunk
	sync.RWMutex
	Handles []SparkleDFS.ChunkHandle
}

type chunkInfo struct {
	sync.RWMutex
	Path      SparkleDFS.Path            //chunk属于哪个path上的文件 todo//可以考虑多个锁（分段锁）
	Locations []SparkleDFS.ServerAddress `json:"-"` // 这个chunk存在于哪些服务器
	Primary   SparkleDFS.ServerAddress   // 这个chunk的主chunkserver
	Expire    time.Time                  // lease Expire time
	Version   SparkleDFS.ChunkVersion
	Checksum  SparkleDFS.Checksum //校验值
}

// CreateChunk 为path创建一个新的chunk。由addrs表示的chunk服务器返回新chunk的ChunkHandle，并且成功创建chunk的服务器addrs
func (c *ChunkManager) CreateChunk(path SparkleDFS.Path, addrs []SparkleDFS.ServerAddress) (SparkleDFS.ChunkHandle, []SparkleDFS.ServerAddress, error) {
	c.Lock()
	defer c.Unlock()
	curChunkHandle := c.CurChunkHandle
	c.CurChunkHandle++
	curFileInfo, ok := c.Files[path]
	if !ok { //如果是新创建的文件
		curFileInfo = &FileInfo{
			Handles: make([]SparkleDFS.ChunkHandle, 0),
		}
		c.Files[path] = curFileInfo
	}
	//吧新curChunkHandle更新到fileInfo
	curFileInfo.Handles = append(curFileInfo.Handles, curChunkHandle)
	//创建新的chunkInfo
	curChunkInfo := &chunkInfo{Path: path}
	//远程调用rpc到ChunkServer的RPCCreateChunk方法
	errList := ""                          //将所有错误信息整合在一个string
	var success []SparkleDFS.ServerAddress //rpc成功调用的服务器列表
	for _, addr := range addrs {
		var r SparkleDFS.CreateChunkReply
		err := util.RPCCall(addr, SparkleDFS.RPC_ChunkServer_CreateChunk, SparkleDFS.CreateChunkArg{Handle: curChunkHandle}, &r)
		if err != nil {
			errList += err.Error() + ";"
		} else { //rpc成功
			success = append(success, addr)
			curChunkInfo.Locations = append(curChunkInfo.Locations, addr)
		}
	}
	c.Chunks[curChunkHandle] = curChunkInfo
	if errList == "" {
		return curChunkHandle, success, nil
	} else {
		c.replicasNeedList = append(c.replicasNeedList, curChunkHandle) //如果有rpc失败，如何处理？？ todo
		return curChunkHandle, success, fmt.Errorf(errList)
	}
}

//GetChunk 找出这个文件的第index（偏移）个chunk的handle
func (c *ChunkManager) GetChunk(path SparkleDFS.Path, index SparkleDFS.ChunkIndex) (SparkleDFS.ChunkHandle, error) { //只根据pth找到对应文件，index指的是第几个chunk
	c.RLock()
	defer c.RUnlock()
	if v, ok := c.Files[path]; ok {
		if index < 0 || int(index) >= len(v.Handles) {
			return -1, fmt.Errorf("Invalid index for %v[%v]", path, index)
		}
		return v.Handles[index], nil
	} else {
		return -1, fmt.Errorf("Path{%v},index{%v},chunk not Found", path, index)
	}
}

// RemoveChunks RemoveChunks在chunkmanager里删除断开连接的块，所以不用rpc删除
//如果块的副本数小于MinNumsOfReplicas，将它添加到需要列表中
func (c *ChunkManager) RemoveChunks(handles []SparkleDFS.ChunkHandle, server SparkleDFS.ServerAddress) error {
	errList := ""
	c.Lock()
	defer c.Unlock()
	for _, handle := range handles {
		c.RLock()
		curChunkInfo, ok := c.Chunks[handle]
		c.RUnlock()
		if !ok {
			continue
		}
		curChunkInfo.Lock()
		delIndex := 0 //进行覆盖无效位删除 这种删除性能比append高
		for j := range curChunkInfo.Locations {
			if curChunkInfo.Locations[j] != server {
				curChunkInfo.Locations[delIndex] = curChunkInfo.Locations[j]
				delIndex++
			}
		}
		curChunkInfo.Locations = curChunkInfo.Locations[:delIndex]
		replicaNums := len(curChunkInfo.Locations)

		curChunkInfo.Expire = time.Now() // todo

		curChunkInfo.Unlock()
		if replicaNums < SparkleDFS.Num_MinOfReplicas {
			c.Lock()
			c.replicasNeedList = append(c.replicasNeedList, handle)
			c.Unlock()
			if replicaNums == 0 { //没有副本了，丢失chunk
				log.Error("lose all replica of ", handle)
				errList += fmt.Sprintf("Lose all replicas of chunk %v;", handle)
			}
		}
	}
	if errList == "" {
		return nil
	} else {
		return fmt.Errorf(errList)
	}

}

// RegisterReplica 只在chunkManager里添加副本信息，没有发送rpc  todo 这里可以不用锁？
func (c *ChunkManager) RegisterReplica(handle SparkleDFS.ChunkHandle, addr SparkleDFS.ServerAddress) error {
	c.RLock()
	curChunkInfo, ok := c.Chunks[handle]
	c.RUnlock()
	if !ok {
		return fmt.Errorf("ChunkHandle {%v} not Found", handle)
	}
	curChunkInfo.Lock()
	defer curChunkInfo.Unlock()
	curChunkInfo.Locations = append(curChunkInfo.Locations, addr)
	return nil
}

//GetReplicas 返回该handle副本的所有服务器addr
func (c *ChunkManager) GetReplicas(handle SparkleDFS.ChunkHandle) ([]SparkleDFS.ServerAddress, error) {
	c.RLock()
	curChunkInfo, ok := c.Chunks[handle]
	c.RUnlock()
	if !ok {
		return nil, fmt.Errorf("ChunkHandle {%v} not Found", handle)
	}
	return curChunkInfo.Locations, nil
}

//GetLeaseHolder 返回持有块租约的chunkserver，并检查租约是否到期，到期的话更新所有租约，将落后版本的加入垃圾回收
//(即主)和租约的到期时间。如果没有人拥有租约，则授予其选择的副本一份。 返回过期的服务器
func (cm *ChunkManager) GetLeaseHolder(handle SparkleDFS.ChunkHandle) (*SparkleDFS.Lease, []SparkleDFS.ServerAddress, error) {
	cm.RLock()
	ckInfo, ok := cm.Chunks[handle]
	cm.RUnlock()
	if !ok {
		return nil, nil, fmt.Errorf("invalid chunk handle %v", handle)
	}
	ckInfo.Lock()
	defer ckInfo.Unlock()
	lease := &SparkleDFS.Lease{}
	var overdueServerAddrs []SparkleDFS.ServerAddress
	var overdueServerAddrsLock sync.Mutex
	if ckInfo.Expire.Before(time.Now()) { //如果租约过期了,版本+1，rpc向所有副本服务器 版本检查该chunk todo
		ckInfo.Version++
		args := SparkleDFS.CheckVersionArg{Handle: handle, Version: ckInfo.Version}
		var wg sync.WaitGroup
		var newLocations []SparkleDFS.ServerAddress
		var newLocationsLock sync.Mutex

		for _, addr := range ckInfo.Locations {
			wg.Add(1)
			var reply SparkleDFS.CheckVersionReply
			//并行需要加锁
			go func(addr SparkleDFS.ServerAddress) {
				err := util.RPCCall(addr, SparkleDFS.RPC_ChunkServer_CheckVersion, args, &reply)
				log.Infof("ChunkServer_CheckVersion to %v{version:%v,Handle:%v}", addr, args.Version, args.Handle)
				if err == nil && reply.Overdue == false {
					newLocationsLock.Lock()
					newLocations = append(newLocations, addr)
					newLocationsLock.Unlock()
				} else { //err通讯失败或者已过期都视为需要清理的
					if err != nil {
						log.Warning("GetLeaseHolder RPCCall err:", err)
					}
					log.Warningf("detect overdue chunk %v in %v", handle, addr)
					overdueServerAddrsLock.Lock()
					overdueServerAddrs = append(overdueServerAddrs, addr) //todo 通信失败也视为过期？？
					overdueServerAddrsLock.Unlock()
				}
				wg.Done()
			}(addr)
		}
		wg.Wait() //
		ckInfo.Locations = newLocations
		log.Warning(handle, " lease location ", ckInfo.Locations)
		ckInfo.Expire = time.Now()
		if len(ckInfo.Locations) < SparkleDFS.Num_MinOfReplicas { //如果副本数量不够，添加到needlist
			cm.Lock()
			cm.replicasNeedList = append(cm.replicasNeedList, handle)
			cm.Unlock()
			if len(ckInfo.Locations) == 0 {
				// !! ATTENTION !!
				ckInfo.Version--
				return nil, nil, fmt.Errorf("no replica Handle{%v},already Add into replicasNeedList", handle) //todo 副本复制的优先级
			}
		}
		// TODO choose primary, !!error handle no replicas!! 没有主服务器会
		ckInfo.Primary = ckInfo.Locations[0] //
		ckInfo.Expire = time.Now().Add(SparkleDFS.Time_LeaseExpire)
	}
	//
	lease.Primary = ckInfo.Primary
	lease.Expire = ckInfo.Expire
	lease.Secondaries = make([]SparkleDFS.ServerAddress, 0)
	for _, addr := range ckInfo.Locations {
		if ckInfo.Primary != addr {
			lease.Secondaries = append(lease.Secondaries, addr)
		}
	}
	return lease, overdueServerAddrs, nil

}
