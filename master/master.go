package master

import (
	"SparkleDFS"
	"SparkleDFS/util"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"net"
	"net/rpc"
	"os"
	"path"
	"time"
)

type Master struct {
	address        SparkleDFS.ServerAddress // master的地址
	RPCConnPool    map[SparkleDFS.ServerAddress]*rpc.Client
	serverPathRoot string //master在本地磁盘元数据持久化的文件目录
	l              net.Listener
	shutdown       chan struct{} //todo 可以复用消息吗？？ NewAndServe就有两个调用
	dead           bool          //set to ture if server is shuntdown

	NM  *nameSpaceManager   `json:"nameSpaceManager"` //命名空间树 模块
	CM  *ChunkManager       `json:"ChunkManager"`     //chunk的metadata
	CSM *chunkServerManager `json:"-"`                // "-"表示不进行序列化 //管理chunk服务器的模块

}

const (
	MetaFile = "Master.meta"
)

type IMaster interface {
	initMetadata()
	loadMeta() error
	StoreMeta() error
	Shutdown()
	//serverCheck 根据上一次心跳时间检查所有chunkserver，然后删除所有断开连接的服务器信息
	serverCheck() error
	//reReplication 执行chunk重复制，每次调用由源ckserver复制数据到另外一个ckserver，并在master上注册信息。ck应该被锁定在顶级调用者中
	reReplication(handle SparkleDFS.ChunkHandle) error
	//RPCHeartbeat 由chunkServer调用，接受来自chunkserver的心跳，更新该chunkserver的上次心跳时间，返回垃圾chunk列表。
	//如果是第一次心跳还需要叫chunkServer报告自己有的handle
	RPCHeartbeat(args SparkleDFS.HeartbeatArg, reply *SparkleDFS.HeartbeatReply) error
	//RPCGetPrimaryAndSecondaries 由客户端调用，得到主备chunkServer位置
	//下层 todo Lease版本检查是否可以放到后台定期检查，避免客户端rpc来的时候检查延迟太高
	RPCGetPrimaryAndSecondaries(args SparkleDFS.GetPrimaryAndSecondariesArg, reply *SparkleDFS.GetPrimaryAndSecondariesReply) error
	//暂无实现
	RPCExtendLease(args SparkleDFS.ExtendLeaseArg, reply *SparkleDFS.ExtendLeaseReply) error
	//RPCGetReplicas 由客户端调用，返回该handle副本的所有服务器addr
	RPCGetReplicas(args SparkleDFS.GetReplicasArg, reply *SparkleDFS.GetReplicasReply) error

	//以下均是管理信息的修改，不涉及实际chunks，均由客户端调用
	RPCCreateFile(args SparkleDFS.CreateFileArg, reply *SparkleDFS.CreateFileReply) error
	RPCDeleteFile(args SparkleDFS.DeleteFileArg, reply *SparkleDFS.DeleteFileReply) error
	RPCRenameFile(args SparkleDFS.RenameFileArg, reply *SparkleDFS.RenameFileReply) error
	RPCMkdir(args SparkleDFS.MkdirArg, reply *SparkleDFS.MkdirReply) error
	RPCList(args SparkleDFS.ListArg, reply *SparkleDFS.ListReply) error
	RPCGetFileInfo(args SparkleDFS.GetFileInfoArg, reply *SparkleDFS.GetFileInfoReply) error
	RPCGetChunkHandle(args SparkleDFS.GetChunkHandleArg, reply *SparkleDFS.GetChunkHandleReply) error
}

//NewAndServe 初始化master
//Background Task 定期检查掉线服务器、以及进行元数据的持久化
func NewAndServe(address SparkleDFS.ServerAddress, serverPathRoot string) *Master {
	m := &Master{address: address, serverPathRoot: serverPathRoot, shutdown: make(chan struct{})}
	rpcServer := rpc.NewServer()
	l, err := net.Listen("tcp4", string(address))
	if err != nil {
		log.Fatal("Listen err:", err)
	}
	rpcServer.Register(m) //注册Master的RPC方法
	m.l = l
	m.initMetadata()
	//RPC Handle
	go func() {
		for {
			select {
			case <-m.shutdown:
				return
			default:
				conn, err := l.Accept()
				if err != nil {
					log.Errorf("Accept err:%v", err)
					continue
				}
				go func() {
					rpcServer.ServeConn(conn)
					conn.Close()
				}()
			}
		}
	}()
	//Background Task 定期检查掉线服务器、以及进行元数据的持久化
	go func() {
		//checkTicker := time.Tick(SparkleDFS.Time_ServerCheckInterval)
		storeTicker := time.Tick(SparkleDFS.Time_MasterStoreInterval)
		for {
			var err error
			select {
			case <-m.shutdown:
				return
			//case <-checkTicker:
			//	err = m.serverCheck()
			case <-storeTicker:
				err = m.StoreMeta()
			}
			if err != nil {
				log.Error("Background error ", err)
			}
		}

	}()

	log.Infof("Master is running now. addr = %v", address)
	return m
}
func (m *Master) initMetadata() {
	m.CSM = newChunkServerManager()
	m.CM = newChunkManager()
	m.NM = newNamespaceManager()
	m.loadMeta()
}
func (m *Master) loadMeta() error {
	metaFile, err := os.OpenFile(MetaFile, os.O_CREATE|os.O_RDONLY, SparkleDFS.FilePerm)
	defer metaFile.Close()
	if err != nil {
		return err
	}
	encoder := json.NewDecoder(metaFile)
	err = encoder.Decode(m)
	if err != nil {
		log.Warning("loadMeta err:", err)
	}
	return err
}
func (m *Master) StoreMeta() error {
	metaFileName := path.Join(m.serverPathRoot, MetaFile)
	metaFile, err := os.OpenFile(metaFileName, os.O_CREATE|os.O_WRONLY, SparkleDFS.FilePerm)
	defer metaFile.Close()
	if err != nil {
		return err
	}
	encoder := json.NewEncoder(metaFile)
	return encoder.Encode(m)

}

//serverCheck根据上一次心跳时间检查所有chunkserver，然后删除所有断开连接的服务器信息
func (m *Master) serverCheck() error {

	delAddrs := m.CSM.DetectDeadServers()
	for _, delAddr := range delAddrs {
		delHandles, err := m.CSM.RemoveServer(delAddr)
		if err != nil { // todo 中途出错，后面就停了
			return err
		}
		err = m.CM.RemoveChunks(delHandles, delAddr)
		if err != nil {
			return err
		}
	}

	needReplicaHandles := m.CM.replicasNeedList
	if needReplicaHandles != nil {
		log.Info("Master needs", needReplicaHandles)
		m.CM.RLock()
		for _, curHandle := range needReplicaHandles {
			if curChunkInfo, ok := m.CM.Chunks[curHandle]; ok {
				if curChunkInfo.Expire.Before(time.Now()) { //在重新备份期间不要给予租赁
					curChunkInfo.Lock()
					err := m.reReplication(curHandle)
					log.Warning("master reReplication err :", err)
					curChunkInfo.Unlock()
				}
			} else {
				log.Warning("needReplicaHandle cant find:", curHandle)
			}
			m.reReplication(curHandle)
		}
		m.CM.RUnlock()

	}
	return nil

}

func (m *Master) Shutdown() {
	if !m.dead {
		m.dead = true
		close(m.shutdown)
		log.Warning(m.address, "is shutting down")
		m.l.Close()
	}
	err := m.StoreMeta()
	if err != nil {
		log.Warning("err in storing metadata:", err)
	}
}

//reereplication 执行重复制，ck应该被锁定在顶级调用者中
//在复制期间，新租约将不会被批准
func (m *Master) reReplication(handle SparkleDFS.ChunkHandle) error {
	originServer, targetServer, err := m.CSM.ChooseReReplication(handle)
	if err != nil {
		return err
	}
	log.Warningf("allocate new chunk %v from %v to %v", handle, originServer, targetServer)
	//在目标服务器创造chunk
	var createRelpy SparkleDFS.CreateChunkReply
	err = util.RPCCall(
		targetServer,
		SparkleDFS.RPC_ChunkServer_CreateChunk,
		SparkleDFS.CreateChunkArg{Handle: handle},
		&createRelpy,
	)
	if err != nil {
		return err
	}
	//只会源服务器传送数据
	var sr SparkleDFS.SendCopyReply
	err = util.RPCCall(originServer,
		SparkleDFS.RPC_ChunkServer_SendCopy,
		SparkleDFS.SendCopyArg{handle, originServer},
		&sr)
	if err != nil {
		return err
	}
	//操作成功，在Master上注册信息
	err = m.CM.RegisterReplica(handle, targetServer)
	if err != nil {
		return err
	}
	m.CSM.AddChunk([]SparkleDFS.ServerAddress{targetServer}, handle)
	return nil
}

func (m *Master) RPCHeartbeat(args SparkleDFS.HeartbeatArg, reply *SparkleDFS.HeartbeatReply) error {
	isFirstBeat := m.CSM.Heartbeat(args.Address, reply) //注册了chunkserver
	curServerAddr := args.Address
	//for _, handle := range args.LeaseExtensions {// todo 大约有bug
	//	continue
	//	// todo ATTENTION !! dead lock
	//	//m.cm.ExtendLease(handle, args.Address)
	//}
	if isFirstBeat { //如果是第一个，需要注册该服务器拥有的chunk
		var reply SparkleDFS.ReportSelfReply
		err := util.RPCCall(args.Address, SparkleDFS.RPC_ChunkServer_ReportSelf, SparkleDFS.ReportSelfArg{}, &reply)
		if err != nil {
			log.Warning("RPCCall ERR:", err)
			return err
		}
		for _, rpcChunkInfo := range reply.Chunks {
			m.CM.RLock()
			ckInfo, ok := m.CM.Chunks[rpcChunkInfo.Handle]
			m.CM.RUnlock()
			if !ok {
				continue // todo 如果没有注册过chunk，为什么就直接跳过了,因为创建chunkmaster是最先知道的，它不可能丢失
			}
			version := ckInfo.Version

			if ckInfo.Version == version { //对比master的版本，master的版本始终是最新的
				log.Infof("Master receive chunk %v from %v", rpcChunkInfo.Handle, args.Address)
				m.CM.RegisterReplica(rpcChunkInfo.Handle, curServerAddr)
				m.CSM.AddChunk([]SparkleDFS.ServerAddress{curServerAddr}, rpcChunkInfo.Handle)
			} else {
				log.Infof("Master discard Chunk{%v} because of behindhand Chunk", rpcChunkInfo.Handle)
			}
		}
	}
	return nil
}
func (m *Master) RPCGetPrimaryAndSecondaries(args SparkleDFS.GetPrimaryAndSecondariesArg, reply *SparkleDFS.GetPrimaryAndSecondariesReply) error {
	lease, overdueServers, err := m.CM.GetLeaseHolder(args.Handle) // todo Lease版本检查是否可以放到后台定期检查，避免客户端rpc来的时候检查延迟太高
	if err != nil {
		return err
	}
	for _, v := range overdueServers { //将chunk持有版本低于master的服务器加入垃圾清理,这些chunk位于该server的信息被清理 todo 在后续调用rpc清理？
		m.CSM.AddGarbage(v, args.Handle)
	}
	reply.Secondaries = lease.Secondaries
	reply.Primary = lease.Primary
	reply.Expire = lease.Expire
	return nil
}
func (m *Master) RPCMkdir(args SparkleDFS.MkdirArg, reply *SparkleDFS.MkdirReply) error {
	return m.NM.Mkdir(args.Path)
}
func (m *Master) RPCList(args SparkleDFS.ListArg, reply *SparkleDFS.ListReply) error {
	dirInfos, err := m.NM.List(args.Path)
	reply.DirInfos = dirInfos
	return err
}
func (m *Master) RPCCreateFile(args SparkleDFS.CreateFileArg, reply *SparkleDFS.CreateFileReply) error {
	return m.NM.Create(args.Path)
}
func (m *Master) RPCDeleteFile(args SparkleDFS.DeleteFileArg, reply *SparkleDFS.DeleteFileReply) error {
	return m.NM.Delete(args.Path)
}
func (m *Master) RPCRenameFile(args SparkleDFS.RenameFileArg, reply *SparkleDFS.RenameFileReply) error {
	return m.NM.Rename(args.Origin, args.Target)
}
func (m *Master) RPCGetFileInfo(args SparkleDFS.GetFileInfoArg, reply *SparkleDFS.GetFileInfoReply) error {
	parents, father, err := m.NM.lockParents(args.Path, false)
	defer m.NM.unlockParents(parents)
	if err != nil {
		return fmt.Errorf("Can't find path:{%v}", args.Path)
	}
	file, ok := father.Children[parents[len(parents)-1]]
	if !ok {
		return fmt.Errorf("File %v does not exist", args.Path)
	}
	file.Lock()
	defer file.Unlock()
	reply.IsDir = file.IsDir
	reply.Length = file.Length
	reply.Chunks = file.Chunks
	return nil
}

//RPCGetChunkHandle 由path和index找handle 如果请求的索引比该路径的块数多一个，则创建一个。 todo 为什么会请求偏移多一个
func (m *Master) RPCGetChunkHandle(args SparkleDFS.GetChunkHandleArg, reply *SparkleDFS.GetChunkHandleReply) error {
	parents, curNSTree, err := m.NM.lockParents(args.Path, true)
	defer m.NM.unlockParents(parents)
	if err != nil {
		return err
	}
	if curNSTree.Chunks == int64(args.Index) {
		curNSTree.Chunks++
		addrs, err := m.CSM.ChooseServers(3)
		if err != nil {
			return err
		}
		reply.Handle, addrs, err = m.CM.CreateChunk(args.Path, addrs)
		log.Debugf("Master call ChunkServer CreateChunk %v on %v success!", reply.Handle, addrs)
		if err != nil {
			// WARNING
			log.Warning("[ignored] An ignored error in RPCGetChunkHandle when create ", err, " in create chunk ", reply.Handle)
		}
		m.CSM.AddChunk(addrs, reply.Handle)
	} else {
		reply.Handle, err = m.CM.GetChunk(args.Path, args.Index)
	}
	return err
}
func (m *Master) RPCGetReplicas(args SparkleDFS.GetReplicasArg, reply *SparkleDFS.GetReplicasReply) error {
	servers, err := m.CM.GetReplicas(args.Handle)
	if err != nil {
		return err
	}
	reply.Locations = servers // todo 这里直接赋值会有问题吗
	return nil
}

//如果承租人不是任何人或请求者，RPCExtendLease将扩展块的租约。
func (m *Master) RPCExtendLease(args SparkleDFS.ExtendLeaseArg, reply *SparkleDFS.ExtendLeaseReply) error {
	return nil // todo
}
