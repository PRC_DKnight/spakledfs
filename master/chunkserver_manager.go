package master

import (
	"SparkleDFS"
	"SparkleDFS/util"
	"fmt"
	log "github.com/sirupsen/logrus"
	"sync"
	"time"
)

type chunkServerManager struct {
	sync.RWMutex
	servers map[SparkleDFS.ServerAddress]*chunkServerInfo
}

type IChunkServerManager interface {
	Heartbeat(addr SparkleDFS.ServerAddress, reply *SparkleDFS.HeartbeatReply) bool //ChunkServerManager依靠心跳来进行新服务器（或是恢复的）的注册，相当于这就是一个addNewServer方法
	AddChunk(addrs []SparkleDFS.ServerAddress, handle SparkleDFS.ChunkHandle)       //在ChunkServerManager上注册chunk，应该是在chunkManager的CreateChunk()方法返回过后调用
	AddGarbage(addr SparkleDFS.ServerAddress, handle SparkleDFS.ChunkHandle)
	//ChooseReReplication 选择执行chunk副本数小于gfs的MinNumsOfReplicas时调用的重新复制的服务器。返回两个服务器地址，主服务器将调用'from'发送副本到'to'
	ChooseReReplication(handle SparkleDFS.ChunkHandle) (from, to SparkleDFS.ServerAddress, err error)
	ChooseServers(ReplicaNum int) ([]SparkleDFS.ServerAddress, error)             //ChooseServers返回用于存储创建新块时调用的新块的服务器
	DetectDeadServers() []SparkleDFS.ServerAddress                                //DetectDeadServers根据最近的心跳时间检测断开连接的服务器
	RemoveServer(addr SparkleDFS.ServerAddress) ([]SparkleDFS.ChunkHandle, error) //RemoveServers删除断开连接的服务器的metedata，它返回服务器持有的块
}

func newChunkServerManager() *chunkServerManager {
	log.Info("-----------new chunk server manager")
	return &chunkServerManager{servers: make(map[SparkleDFS.ServerAddress]*chunkServerInfo)}
}

type chunkServerInfo struct {
	lastHeartbeat time.Time                       //上一个心跳的时间
	chunks        map[SparkleDFS.ChunkHandle]bool // 一个用作set集合的map，表示这个ChunkHandle是否在该chunk服务器上
	garbage       []SparkleDFS.ChunkHandle        // todo 需要清理的chunkid,版本相较于master低
}

func NewChunkServerInfo() *chunkServerInfo {
	return &chunkServerInfo{
		lastHeartbeat: time.Time{},
		chunks:        make(map[SparkleDFS.ChunkHandle]bool),
		garbage:       nil,
	}
}

//Heartbeat 回复是否是该服务器的第一个心跳（自断联以来）,注册chunkServerInfo
func (csm *chunkServerManager) Heartbeat(addr SparkleDFS.ServerAddress, reply *SparkleDFS.HeartbeatReply) bool {
	csm.Lock()
	defer csm.Unlock()
	sv, ok := csm.servers[addr]
	if !ok {
		log.Info("New chunk server" + addr)
		csm.servers[addr] = &chunkServerInfo{time.Now(), make(map[SparkleDFS.ChunkHandle]bool), nil}
		return true // todo 没有更新chunk？
	} else {
		// send garbage todo 回复就只有这个？
		reply.Garbage = csm.servers[addr].garbage
		csm.servers[addr].garbage = make([]SparkleDFS.ChunkHandle, 0)
		sv.lastHeartbeat = time.Now()
		return false
	}
}

//AddChunk 往目标服务器注册handle
func (csm *chunkServerManager) AddChunk(addrs []SparkleDFS.ServerAddress, handle SparkleDFS.ChunkHandle) { //当部分服务器已下线，怎么处理？ todo
	csm.Lock()
	defer csm.Unlock()
	for _, addr := range addrs {
		serverInfo, ok := csm.servers[addr]
		if !ok {
			log.Errorf("Add Chunk{%v} to serverAddr:{%v} has been disconnected", handle, addr)
		}

		serverInfo.chunks[handle] = true //todo!!!! panic: runtime error: invalid memory address or nil pointer dereference
		//todo 当打断点时，后台已经清理一时没有心跳的serverInfo，所以serverInfo.chunks[handle]是空指针
	}
}
func (csm *chunkServerManager) AddGarbage(addr SparkleDFS.ServerAddress, handle SparkleDFS.ChunkHandle) {
	csm.Lock()
	defer csm.Unlock()
	serverInfo, ok := csm.servers[addr]
	if !ok {
		log.Errorf("Master Add Garbage{%v} to serverAddr:{%v} has been disconnected", handle, addr)
	}
	serverInfo.garbage = append(serverInfo.garbage, handle)
}

//ChooseServers 随机挑选，没有负载均衡，就近原则
func (csm *chunkServerManager) ChooseServers(ReplicaNum int) ([]SparkleDFS.ServerAddress, error) {
	if ReplicaNum > len(csm.servers) {
		return nil, fmt.Errorf("there is no engouh online servers for Replicate")
	}
	csm.RLock()
	defer csm.RUnlock()
	randServerIndexs, err := util.RandKinN(ReplicaNum, len(csm.servers))
	if err != nil {
		return nil, err
	}
	var allServerAddrs []SparkleDFS.ServerAddress
	for a, _ := range csm.servers {
		allServerAddrs = append(allServerAddrs, a)
	}
	var randServers []SparkleDFS.ServerAddress
	for _, randServerIndex := range randServerIndexs {
		randServers = append(randServers, allServerAddrs[randServerIndex])
	}
	return randServers, nil
}
func (csm *chunkServerManager) DetectDeadServers() []SparkleDFS.ServerAddress { //DetectDeadServers根据最近的心跳时间检测断开连接的服务器
	csm.RLock()
	defer csm.RUnlock()
	curTime := time.Now()
	var deadServers []SparkleDFS.ServerAddress
	for serverAddr, serverInfo := range csm.servers {
		if serverInfo.lastHeartbeat.Add(SparkleDFS.Time_ServerTimeOut).Before(curTime) {
			deadServers = append(deadServers, serverAddr)
		}
	}
	return deadServers
}

//RemoveServer 删除断开连接的服务器的metedata，它返回服务器持有的块
func (csm *chunkServerManager) RemoveServer(addr SparkleDFS.ServerAddress) ([]SparkleDFS.ChunkHandle, error) {
	csm.Lock()
	defer csm.Unlock()
	serverInfo, ok := csm.servers[addr]
	if !ok {
		return nil, fmt.Errorf("disconnect serverAddr{%v} is not found,may already removed", addr)
	}
	var ChunkHandles []SparkleDFS.ChunkHandle
	for k, v := range serverInfo.chunks {
		if v {
			ChunkHandles = append(ChunkHandles, k)
		}
	}

	delete(csm.servers, addr)
	log.Infof("disconnect serverAddr{%v} is removed", addr)
	return ChunkHandles, nil

}

//ChooseReReplication 选择重新复制的服务器
//当chunk的副本数小于MinNumReplicas时调用。
//返回两个服务器地址，主服务器将调用'from'发送副本到'to' todo 一次调用只返回一个可提供副本存储的服务器
func (csm *chunkServerManager) ChooseReReplication(handle SparkleDFS.ChunkHandle) (from, to SparkleDFS.ServerAddress, err error) {
	from = ""
	to = ""
	err = nil
	for a, v := range csm.servers { //todo 遍历map找到一个算一个，不是随机，而是map遍历顺序，可以改进
		if v.chunks[handle] {
			from = a
		} else {
			to = a
		}
		if from != "" && to != "" {
			return
		}
	}
	err = fmt.Errorf("No enough server for replica %v", handle)
	return
}
