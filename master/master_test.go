package master

import (
	"SparkleDFS"
	"SparkleDFS/util"
	"fmt"
	"testing"
)

func Test_init(t *testing.T) {
	m := NewAndServe("0.0.0.0:5556", "./")
	//select{
	//
	//}
	m.NM.Mkdir("/Root")
	m.NM.Mkdir("/Root/mm")
	m.NM.Mkdir("/Root/mm/gg")
	m.NM.Create("/Root/mm/gg/mg.db")
	ps, ced, err := m.NM.lockParents("/Root/mm/gg/mg.db", false)
	fmt.Println(ps, ced, err)
	m.NM.Create("/Root/my.db")
	m.StoreMeta()
	fmt.Println(m.serverPathRoot)
}
func Test_RPC_Mkdir(t *testing.T) {
	args := SparkleDFS.MkdirArg{Path: "/sda/by"}
	var reply SparkleDFS.MkdirReply
	err := util.RPCCall("0.0.0.0:5556", SparkleDFS.RPC_Master_Mkdir, args, &reply)
	fmt.Println(err)
}
