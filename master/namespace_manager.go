package master

import (
	"SparkleDFS"
	"fmt"
	log "github.com/sirupsen/logrus"
	"strings"
	"sync"
)

//命名空间树中的有效节点，要么是一个文件路径，要么是一个文件目录路径
type nameSpaceManager struct {
	//GFS 使用一个查找表来保存文件路径到其元信息的映射，并且使用前缀压缩（prefix compression）的技术来优化存储
	//（这么看来有可能使用了压缩过的前缀树作为数据结构？）
	Root     *nsTree
	serialCt int //序列化过的节点计数
}
type nsTree struct {
	sync.RWMutex

	// if it is a directory
	IsDir    bool               `json:"IsDir"`
	Children map[string]*nsTree `json:"Children"`

	// if it is a file
	Length int64 `json:"Length"`
	Chunks int64 `json:"Chunks"`
}

func newNamespaceManager() *nameSpaceManager {
	nm := &nameSpaceManager{
		Root: &nsTree{IsDir: true,
			Children: make(map[string]*nsTree)},
	}
	log.Info("-----------new namespace manager")
	return nm
}

func newNsTree(isDir bool) *nsTree {
	return &nsTree{IsDir: isDir, Children: make(map[string]*nsTree)}
}

type INameSpaceManager interface {
	Create(p SparkleDFS.Path) error              //创建文件
	Delete(p SparkleDFS.Path) error              //删除目录或者文件
	Rename(source, target SparkleDFS.Path) error //重命名，暂不支持
	Mkdir(p SparkleDFS.Path) SparkleDFS.Error    //创建目录

	List(p SparkleDFS.Path) ([]SparkleDFS.PathInfo, error)           //List返回p中的所有文件和目录的信息。
	SeparatePathAndFile(p SparkleDFS.Path) (SparkleDFS.Path, string) //分割最后一个目录（文件）和前面的路径

	//lockParents 给路径上读锁，ReadLockLastRoot标识是否给最后一个目录上锁，一般要写的时候调这个函数不上锁，单独给最后一个目录上读写锁
	lockParents(p SparkleDFS.Path, ReadLockLastRoot bool) ([]string, *nsTree, error)
	unlockParents(ps []string)
}

func (n *nameSpaceManager) Mkdir(p SparkleDFS.Path) error {
	p, dir := n.SeparatePathAndFile(p)
	parents, father, err := n.lockParents(p, true) //不给最后一个目录上读锁 单独上读写锁
	if err != nil {
		return &SparkleDFS.Error{SparkleDFS.ErrorCode_NoneType, err.Error()}
	}
	defer n.unlockParents(parents)
	father.Lock()
	defer father.Unlock()

	//先检查是否存在
	if _, ok := father.Children[dir]; ok {
		return &SparkleDFS.Error{SparkleDFS.ErrorCode_Mkdir_DirAlreadyExists, fmt.Sprintf("%v is already exists in %v", dir, p)}
	}
	father.Children[dir] = newNsTree(true)
	log.Info("Mkdir a new dir:", string(p)+"/"+dir)
	return nil
}

//创造一个空文件（文件节点）
func (n *nameSpaceManager) Create(p SparkleDFS.Path) error {
	p, file := n.SeparatePathAndFile(p)
	parents, father, err := n.lockParents(p, true) //不给最后一个目录上读锁 单独上读写锁
	if err != nil {
		return err
	}
	defer n.unlockParents(parents)
	father.Lock()
	defer father.Unlock()

	//先检查是否存在
	if _, ok := father.Children[file]; ok {
		return fmt.Errorf("%v is already exists in %v", file, p)
	}
	father.Children[file] = newNsTree(false)
	log.Infof("Master Create a File on:", p)
	return nil
}

func (n *nameSpaceManager) Delete(p SparkleDFS.Path) error {
	pts, fileOrDir := n.SeparatePathAndFile(p)
	parents, father, err := n.lockParents(pts, true) //不给最后一个目录上读锁 单独上读写锁
	if err != nil {
		return err
	}
	defer n.unlockParents(parents)
	father.Lock()
	defer father.Unlock()

	//先检查是否存在
	if node, ok := father.Children[fileOrDir]; ok {
		father.Children[SparkleDFS.DeletedFilePrefix+fileOrDir] = node //更名，懒删除
		log.Info("Master NameSpace Delete file:", p)
		delete(father.Children, fileOrDir)
		return nil
	} else {
		return fmt.Errorf("%v is not exists in %v", fileOrDir, p)
	}
}

//暂不支持更名
func (n *nameSpaceManager) Rename(source, target SparkleDFS.Path) error {
	log.Fatal("Unsupported Rename")
	return nil
}

func (n *nameSpaceManager) SeparatePathAndFile(p SparkleDFS.Path) (SparkleDFS.Path, string) {
	for i := len(p) - 1; i >= 0; i-- {
		if p[i] == '/' {
			if i == len(p)-1 {
				if len(p) == 1 {
					return "/", "" //根目录
				}
				log.Fatal("there is a '\\' in last spot") //  "/Root/"这类的不合法，必须是"/Root"
			}
			return p[:i], string(p[i+1:]) //注意 p[:i]不包含右端i
		}
	}
	return "", ""
}

//lockParents 将读锁放在p的父节点上，ReadLockLastRoot决定是否给最后一个节点上读锁（因为可能函数外单独上写锁）。
//它返回所有节点的名称列表，以及最后节点（path的最后目录或文件）的父节点nstreet。如果父节点不存在，也会返回一个错误。
func (n *nameSpaceManager) lockParents(p SparkleDFS.Path, returnLast bool) ([]string, *nsTree, error) { //为甚要加一个参数bool godown
	parents := strings.Split(string(p), "/")[1:] //所有节点,包括最后一个  不加第一个根目录""
	nameSpaceRoot := n.Root
	//todo!!!!!!!!!!!   当访问不存在的目录时会panic！
	if len(parents) > 0 {
		nameSpaceRoot.RLock() //递归上读锁
		for i, name := range parents {
			c, ok := nameSpaceRoot.Children[name]
			if !ok {
				return parents, nameSpaceRoot, &SparkleDFS.Error{SparkleDFS.ErrorCode_NoneType, fmt.Sprintf("Path %s not found", p)}
			}
			if i == len(parents)-1 {
				if returnLast {
					nameSpaceRoot = c
				}
			} else {
				nameSpaceRoot = c
				nameSpaceRoot.RLock()
			}
		}
	}
	return parents, nameSpaceRoot, nil
}

//unlockParents 解锁lockParents返回的目录list,但不解锁最后一个目录
func (n *nameSpaceManager) unlockParents(ps []string) {
	cwd := n.Root
	if len(ps) > 0 {
		cwd.RUnlock()
		for _, name := range ps[:len(ps)-1] {
			c, ok := cwd.Children[name]
			if !ok {
				log.Fatal("error in unlock")
				return
			}
			cwd = c
			cwd.RUnlock()
		}
	}
}

//List返回给定path中的所有文件和目录的信息。
func (n *nameSpaceManager) List(p SparkleDFS.Path) (infos []SparkleDFS.PathInfo, err error) {
	log.Info("list ", p)

	var dir *nsTree
	if p == SparkleDFS.Path("/") {
		dir = n.Root
	} else {
		ps, father, err := n.lockParents(p, true)
		defer n.unlockParents(ps)
		if err != nil {
			return nil, err
		}
		dir = father
	}
	dir.RLock()
	defer dir.RUnlock()

	if !dir.IsDir {
		return nil, fmt.Errorf("Path %s is a file, not directory", p)
	}

	for name, v := range dir.Children {
		infos = append(infos, SparkleDFS.PathInfo{
			Name:   name,
			IsDir:  v.IsDir,
			Length: v.Length,
			Chunks: v.Chunks,
		})
	}
	return infos, nil
}
