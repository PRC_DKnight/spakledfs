package master

import (
	"fmt"
	"testing"
)

func TestLockParents(t *testing.T) {
	n := newNamespaceManager()
	err := n.Mkdir("/Root")
	fmt.Println(err)
	err = n.Mkdir("/A")
	fmt.Println(err)
	err = n.Mkdir("/B")
	fmt.Println(err)
	err = n.Create("/Root/my.db")
	err = n.Create("/ASX.db")
	err = n.Create("/ASX.db")
	err = n.Create("S/ASX.db")
	infos, err := n.List("/")

	fmt.Println(infos)
}
func change(a *nameSpaceManager) {
	a.serialCt = 9
}
